import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

import './location.html';


Template.stages.onRendered(function(){
  showDefaultImg();

  Meteor.call('getCurrentUser', (error, response) => {
    if (error) console.warn(error.reason);
      if (response) Session.set('current_user', response);
  });

})

Template.stages.helpers({
  username: function(){
    let user = Session.get('current_user');
    if (user) return user.username;
  },

  wins: function(){
    Meteor.call('getWins', function(error, response){
      Session.set('show_wins', response);
  });
    let wins = Session.get('show_wins');
    console.log(wins);
    return wins;
  },

  losses: function(){
    Meteor.call('getLosses', function(error, response){
      Session.set('show_losses', response);
  });
    let losses = Session.get('show_losses');
    console.log(losses);
    return losses;
  }
})

Template.stages.events({
  'mouseover #africa'(event){
    displayImg('africa');
  },

  'click #africa'(event){
    FlowRouter.go('/battle/africa');
    Meteor.call('initiateStats', 'AFRICA', (error, response) => {});

  },

  'mouseover #australia'(event){
    displayImg('australia');
  },
  'click #australia'(event){
    FlowRouter.go('/battle/australia');
    Meteor.call('initiateStats', 'AUSTRALIA', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  },

  'mouseover #asia'(event){
    displayImg('asia');
  },
  'click #asia'(event){
    FlowRouter.go('/battle/asia');
    Meteor.call('initiateStats', 'ASIA', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  },

  'mouseover #antartica'(event){
    displayImg('antartica');
  },
  'click #antartica'(event){
    FlowRouter.go('/battle/antartica');
    Meteor.call('initiateStats', 'ANTARTICA', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  },

  'mouseover #europe'(event){
    displayImg('europe');
  },
  'click #europe'(event){
    FlowRouter.go('/battle/europe');
    Meteor.call('initiateStats', 'EUROPE', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  },

  'mouseover #north_america'(event){
    displayImg('north_america');
  },
  'click #north_america'(event){
    FlowRouter.go('/battle/north america');
    Meteor.call('initiateStats', 'NORTH AMERICA', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  },

  'mouseover #south_america'(event){
    displayImg('south_america');
  },
  'click #south_america'(event){
    FlowRouter.go('/battle/south america');
    Meteor.call('initiateStats', 'SOUTH AMERICA', (error, response) => {
      if (error) console.warn(error.reason);
        if (response) console.log(response);
    });
  }


})


const showDefaultImg = () =>
{
  var x = document.getElementById('location_img');
  //console.log(x);
  x.style.backgroundSize = "100% 100%";
  x.style.backgroundImage = "url('/img/locations/earth.jpg')";
}

const displayImg = selectedLoc =>
{
  let x = document.getElementById('location_img');
  x.style.backgroundSize="100% 100%";


  switch(selectedLoc)
  {
    case 'africa':
      //x.style.backgroundImage = "url('../../images/locations/africa.jpg')";
      x.innerHTML = "<img src='/img/locations/africa.jpg'>";
      break;

    case 'australia':
      //x.style.backgroundImage = "url('../../images/locations/australia.jpg')";
      x.innerHTML = "<img  src='/img/locations/australia.jpg'>";
      break;

    case 'asia':
      //x.style.backgroundImage = "url('../../images/locations/asia.jpg')";
      x.innerHTML = "<img src='/img/locations/asia.jpg'>";
      break;

    case 'antartica':
      //x.style.backgroundImage = "url('../../images/locations/antartica.jpg')";
      x.innerHTML = "<img src='/img/locations/antartica.jpg'>";
      break;

    case 'europe':
      //x.style.backgroundImage = "url('../../images/locations/europe.jpg')";
      x.innerHTML = "<img src='/img/locations/europe.jpg'>";
      break;

    case 'north_america':
      //x.style.backgroundImage = "url('../../images/locations/north_america.jpg')";
      x.innerHTML = "<img src='/img/locations/north_america.jpg'>";
      break;

    case 'south_america':
      //x.style.backgroundImage = "url('../../images/locations/south_america.jpg')";
      x.innerHTML = "<img src='/img/locations/south_america.jpg'>";
      break;

    default:
      x.innerHTML= "An image could not be found for "+selectedLoc+"!";
      break;
  }
}

function directToFight(str)
{
  window.location.assign("battle_view.php?continent_id="+str);
}
