import { Template } from 'meteor/templating';

import './register.html';

import RegisterScreen from './components/RegisterScreen.jsx';



Template.register_container.helpers({
  RegisterScreen() {
    return RegisterScreen;
  },

  story_screen: function() {
    FlowRouter.go('/story');
  }
});
