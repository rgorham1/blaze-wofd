import { Template } from 'meteor/templating';
import { Tracker } from 'meteor/tracker'
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'
import { ReactiveVar } from 'meteor/reactive-var'

import './create_character.html';

import '../api/base_characters/BaseCharacters';
import Playable_Character_Attacks from '../api/playable_character_attacks/Playable_Character_Attacks';


function showCharacterMsg()
{
	var desc_txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam est dui, bibendum ut metus a, cursus commodo enim. Mauris laoreet malesuada ullamcorper. In id ligula malesuada, interdum velit sed, ornare metus. Cras gravida nunc sed laoreet tristique. Proin non metus ut neque placerat placerat. Curabitur tristique nisi massa, feugiat laoreet lacus tincidunt eu. Aenean malesuada felis velit, sit amet dignissim nisl ullamcorper eu. Morbi quis porta neque. Fusce aliquet sem in tellus molestie, in rutrum magna commodo. Maecenas tincidunt, justo id lacinia vulputate, leo nisi porttitor sem, sit amet fermentum lectus magna quis felis. Vestibulum condimentum justo orci, at vestibulum sem lobortis vel. Fusce volutpat lectus eu ligula sollicitudin, sed consequat lorem vehicula.";
	var choice = document.getElementById("slct_char_type").value;
	var imageToShow = document.getElementById("display_avatar");
	var attributesToShow = document.getElementById("display_attributes");
	var choiceType;

	switch(choice)
	{
	case "Ninja":
		choiceType = "Ninja";
		desc_txt = "Stealthy assassin trained in the art of deception. It is said that this purveyor of darkness controls the ancient elements.";
		imageToShow.innerHTML = "<img src='/img/avatars/ninja.jpg'>";
		document.getElementById("character_strength_display").innerHTML = "1";       //strength
		document.getElementById("character_strength_display").value = "1";

		document.getElementById("character_intelligence_display").innerHTML = "1";   //intelligence
		document.getElementById("character_intelligence_display").value = "1";

		document.getElementById("character_speed_display").innerHTML = "5";          //speed
		document.getElementById("character_speed_display").value = "5";

		document.getElementById("character_stealth_display").innerHTML = "6";        //stealth
		document.getElementById("character_stealth_display").value = "6";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";

		document.getElementById("character_luck_display").innerHTML = "1";           //luck
		document.getElementById("character_luck_display").value = "1";

		document.getElementById("character_chi_display").innerHTML = "2";            //chi
		document.getElementById("character_chi_display").value = "2";
		break;

	case "Boxer":
		choiceType = "Boxer";
		imageToShow.innerHTML = "<img src='/img/avatars/boxer.jpg'>";
		document.getElementById("character_strength_display").innerHTML = "5";       //strength
		document.getElementById("character_strength_display").value = "5";

		document.getElementById("character_intelligence_display").innerHTML = "3";   //intelligence
		document.getElementById("character_intelligence_display").value = "3";

		document.getElementById("character_speed_display").innerHTML = "3";          //speed
		document.getElementById("character_speed_display").value = "3";

		document.getElementById("character_stealth_display").innerHTML = "1";        //stealth
		document.getElementById("character_stealth_display").value = "1";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";

		document.getElementById("character_luck_display").innerHTML = "2";           //luck
		document.getElementById("character_luck_display").value = "2";

		document.getElementById("character_chi_display").innerHTML = "2";            //chi
		document.getElementById("character_chi_display").value = "2";
		break;

	case "Monk":
		choiceType = "Monk";
		imageToShow.innerHTML = "<img src='/img/avatars/monk.jpg'>";
		document.getElementById("character_strength_display").innerHTML = "1";       //strength
		document.getElementById("character_strength_display").value = "1";

		document.getElementById("character_intelligence_display").innerHTML = "4";   //intelligence
		document.getElementById("character_intelligence_display").value = "4";

		document.getElementById("character_speed_display").innerHTML = "2";          //speed
		document.getElementById("character_speed_display").value = "2";

		document.getElementById("character_stealth_display").innerHTML = "1";        //stealth
		document.getElementById("character_stealth_display").value = "1";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";

		document.getElementById("character_luck_display").innerHTML = "3";           //luck
		document.getElementById("character_luck_display").value = "3";

		document.getElementById("character_chi_display").innerHTML = "5";            //chi
		document.getElementById("character_chi_display").value = "5";
		break;

		case "BJJ":
		choiceType = "BJJ";
		desc_txt = "Grew up in the slums of Brazil and looking for an outlet, turned to JiuJitsu. As a way to escape the violence that surrounded him, he let BJJ consume him. ";
		imageToShow.innerHTML = "<img src='/img/avatars/bjj.jpg'>";
		document.getElementById("character_speed_display").innerHTML = "4";          //speed
		document.getElementById("character_speed_display").value = "4";

		document.getElementById("character_strength_display").innerHTML = "2";       //strength
		document.getElementById("character_strength_display").value = "2";

		document.getElementById("character_luck_display").innerHTML = "5";           //luck
		document.getElementById("character_luck_display").value = "5";

		document.getElementById("character_intelligence_display").innerHTML = "2";   //intelligence
		document.getElementById("character_intelligence_display").value = "2";

		document.getElementById("character_chi_display").innerHTML = "2";            //chi
		document.getElementById("character_chi_display").value = "2";

		document.getElementById("character_stealth_display").innerHTML = "1";        //stealth
		document.getElementById("character_stealth_display").value = "1";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";
		break;

	case "Muay Thai":
		choiceType = "Muay Thai";
		imageToShow.innerHTML = "<img src='/img/avatars/muaythai.jpg'>";
		desc_txt = "Abandoned in a beach paradise somewhere in Thailand, grew up to be anything but soft like the sand that surrounded him. Has a body as solid as the sacred elephant. Have limbs as hard as pure steel.";
		document.getElementById("character_speed_display").innerHTML = "4";          //speed
		document.getElementById("character_speed_display").value = "4";

		document.getElementById("character_strength_display").innerHTML = "4";       //strength
		document.getElementById("character_strength_display").value = "4";

		document.getElementById("character_luck_display").innerHTML = "2";           //luck
		document.getElementById("character_luck_display").value = "2";

		document.getElementById("character_intelligence_display").innerHTML = "2";   //intelligence
		document.getElementById("character_intelligence_display").value = "2";

		document.getElementById("character_chi_display").innerHTML = "3";            //chi
		document.getElementById("character_chi_display").value = "3";

		document.getElementById("character_stealth_display").innerHTML = "1";        //stealth
		document.getElementById("character_stealth_display").value = "1";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";
		break;

	case "Sambo":
		choiceType = "Sambo";
		desc_txt = "Frigid hulk of a man. Can control the ice and withstand radiation after surviving Chernobyl. Bent on path of destruction since losing family in Chernobyl incident. Blames the world.";
		imageToShow.innerHTML = "<img src='/img/avatars/sambo.jpg'>";
		document.getElementById("character_speed_display").innerHTML = "1";          //speed
		document.getElementById("character_speed_display").value = "1";

		document.getElementById("character_strength_display").innerHTML = "6";       //strength
		document.getElementById("character_strength_display").value = "6";

		document.getElementById("character_luck_display").innerHTML = "3";           //luck
		document.getElementById("character_luck_display").value = "3";

		document.getElementById("character_intelligence_display").innerHTML = "1";   //intelligence
		document.getElementById("character_intelligence_display").value = "1";

		document.getElementById("character_chi_display").innerHTML = "4";            //chi
		document.getElementById("character_chi_display").value = "4";

		document.getElementById("character_stealth_display").innerHTML = "1";        //stealth
		document.getElementById("character_stealth_display").value = "1";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";
		break;
	case "MMA":
		choiceType = "MMA";
		desc_txt = "\Looking to master all styles to become best fighter.  All around good fighter,  well versed in various techniques and styles.";
		imageToShow.innerHTML = "<img src='/img/avatars/mma.jpg'>";

		document.getElementById("character_speed_display").innerHTML = "3";          //speed
		document.getElementById("character_speed_display").value = "3";

		document.getElementById("character_strength_display").innerHTML = "3";       //strength
		document.getElementById("character_strength_display").value = "3";

		document.getElementById("character_luck_display").innerHTML = "2";           //luck
		document.getElementById("character_luck_display").value = "2";

		document.getElementById("character_intelligence_display").innerHTML = "2";   //intelligence
		document.getElementById("character_intelligence_display").value = "2";

		document.getElementById("character_chi_display").innerHTML = "3";            //chi
		document.getElementById("character_chi_display").value = "3";

		document.getElementById("character_stealth_display").innerHTML = "3";        //stealth
		document.getElementById("character_stealth_display").value = "3";

		//document.getElementById("character_agility_display").innerHTML = "8";        //agility
		//document.getElementById("character_agility_display").value = "8";
		break;
	default:
		document.getElementById("character_strength_display").innerHTML = "0";
		document.getElementById("character_strength_display").innerHTML = "0";       //strength
		document.getElementById("character_intelligence_display").innerHTML = "0";   //intelligence
		document.getElementById("character_speed_display").innerHTML = "0";          //speed
		document.getElementById("character_stealth_display").innerHTML = "0";        //stealth
		//document.getElementById("character_agility_display").innerHTML = "0";        //agility
		document.getElementById("character_luck_display").innerHTML = "0";           //luck
		document.getElementById("character_chi_display").innerHTML = "0";            //chi
	}

	document.getElementById("description").innerHTML = "<b><u>"+choiceType+"</u></b>:<br/>"+desc_txt;

}



function validateCharacterTypeSelected()
{
	var x = document.getElementById("slct_char_type");
	if(x.value != "")
	{
		enableButtons();
		return true;
	}
	return false;
}



function toggleContinueBtn()
{
	//if(validateCharacterName() && validateCharacterTypeSelected())
	if(validateCharacterTypeSelected() && validateAttributeDistribution())
		document.getElementById("btn_continue").disabled = false;
	else
		document.getElementById("btn_continue").disabled = true;
}
//let remainingAttrPts = 5;


function validateAttributeDistribution()
{
	let remainingAttrPts = Session.get('attr_pts');
	if(remainingAttrPts > 0)
		return false;

	return true;
}


function resetPoints()
{
	remainingAttrPts = 5;
	//document.getElementById("atr_pts_remaining").innerHTML = remainingAttrPts.toString();
}

function disableAllButtons()
{
	var allBtns = document.getElementsByTagName("button");
	for(i = 0; i < allBtns.length; i++)
		allBtns[i].disabled = true;
}


function enableButtons()
{
	var allBtns = document.getElementsByTagName("button");
	for(i = 0; i < allBtns.length; i++)
		allBtns[i].disabled = false;
}

function increaseAttributeVal(attributeType)
{
	let pts = Session.get('attr_pts');
	//var x = document.getElementById("atr_pts_remaining");
	//console.log(x);
	if(pts > 0)
	{
		switch(attributeType)
		{
			case "btn_increase_strength":
				updateElements("character_strength_display", "add");
				break;
			case "btn_increase_intelligence":
				updateElements("character_intelligence_display", "add");
				break;
			case "btn_increase_speed":
				updateElements("character_speed_display", "add");
				break;
			case "btn_increase_stealth":
				updateElements("character_stealth_display", "add");
				break;
			/*case "btn_increase_agility":
				updateElements("character_agility_display", "add");
				break;*/
			case "btn_increase_luck":
				updateElements("character_luck_display", "add");
				break;
			case "btn_increase_chi":
				updateElements("character_chi_display", "add");
				break;
			default:
				break;
		}

	}


	toggleContinueBtn();
}

function decreaseAttributeVal(attributeType)
{
	let points;
	//points = document.getElementById("atr_pts_remaining").innerHTML;
	points = Session.get('attr_pts');
	if(points => 0 && points <= 5)
	{
		switch(attributeType)
		{
			case "btn_decrease_strength":
				updateElements("character_strength_display", "subtract");
				break;
			case "btn_decrease_intelligence":
				updateElements("character_intelligence_display", "subtract");
				break;
			case "btn_decrease_speed":
				updateElements("character_speed_display", "subtract");
				break;
			case "btn_decrease_stealth":
				updateElements("character_stealth_display", "subtract");
				break;
			case "btn_decrease_agility":
				updateElements("character_agility_display", "subtract");
				break;
			case "btn_decrease_luck":
				updateElements("character_luck_display", "subtract");
				break;
			case "btn_decrease_chi":
				updateElements("character_chi_display", "subtract");
				break;
			default:
				break;
		}

	}

	toggleContinueBtn();
}

const disableButton = button =>{
	button.disabled = true;
}

const subtract = (elementId, attr) =>
{
	let attr_pts = Session.get('attr_pts');

	let element = document.getElementById(elementId);
	var newVal = parseInt(element.value);
	if(attr < newVal)
	{
		newVal--;
		attr_pts++;


		element.value = newVal.toString();
		element.innerHTML = newVal.toString();
		Session.set('attr_pts', attr_pts);
	}


}

function updateElements(elementId, updateType)
{
	let attr_pts = Session.get('attr_pts');

	if(updateType == "add")
	{

		//console.log(typeof(attr_pts));
		var element = document.getElementById(elementId);
		var newVal = parseInt(element.value);
		//console.log(newVal);
		newVal++;
		//console.log(newVal);
		attr_pts--;
		//remainingAttrPts--;
		//console.log(attr_pts +" left");

		document.getElementById(elementId).value = newVal.toString();
		document.getElementById(elementId).innerHTML = newVal.toString();
		Session.set('attr_pts', attr_pts);
	}

	if(updateType == "subtract")
	{
	 	let character_type= document.getElementById("slct_char_type").value;
		let character = BaseCharacter.findOne({character_description: character_type});
		let element = document.getElementById(elementId);
		let element_name = element.id;

		switch (element_name) {
			case "character_strength_display":
				subtract(elementId, character.strength);
				break;
			case "character_speed_display":
				subtract(elementId, character.speed);
				break;
			case "character_intelligence_display":
				subtract(elementId, character.intelligence);
				break;
			case "character_chi_display":
				subtract(elementId, character.chi);
				break;
			case "character_stealth_display":
				subtract(elementId, character.stealth);
				break;
			case "character_luck_display":
				subtract(elementId, character.luck);
				break;
			default:
				break;

		}



	}
}

function sendCreateRequest()
{
	let charType = document.getElementById("slct_char_type");
	let characterType = charType.options[charType.selectedIndex].value; //equals 'MMA' for example
	let base = BaseCharacter.find({character_description: characterType}).fetch()[0];

	let speed = $('#character_speed_display').val();
	let strength = $('#character_strength_display').val();
	let luck = $('#character_luck_display').val();
	let iq = $('#character_intelligence_display').val();
	let chi = $('#character_chi_display').val();
	let stealth = $('#character_stealth_display').val();

	//new
	user_character =
	{
		'character': characterType,
		'HP': base.hp,
		'AP': base.ap,
		'maxHP': base.hp,
		'maxAP': base.ap,
		'speed': Number(speed),
		'strength': Number(strength),
		'luck': Number(luck),
		'intelligence': Number(iq),
		'chi': Number(chi),
		'stealth': Number(stealth)
	};
	//end new

	return user_character;


}


function getCharacterAttacks(){
	let charType = document.getElementById("slct_char_type");
	let characterType = charType.options[charType.selectedIndex].value;
	let attacks =  Playable_Character_Attacks.find({
			$and: [
			{character: characterType},
			{unique: true}
			],
		}).fetch();



	let current_user = Meteor.userId();
	let attack1 = attacks[0]['attack_name'];
	let attack2 = attacks[1]['attack_name'];
	let attack3 = attacks[2]['attack_name'];

	Meteor.users.update(
	      current_user,
	      { $set:
	      	{
	      		"profile.character.attack_1": attack1,
	      		"profile.character.attack_2": attack2,
	      		"profile.character.attack_3": attack3
	      	}
	      }
	    );

}

/*
const getMoralCharacters = morals =>{
	let pc = BaseCharacter.find({
			good_evil_flag: {$in: [ morals , "both" ]}
		}).fetch();
	console.log(pc);
}
*/


Template.create_character.onRendered(function(){
	disableAllButtons();
	toggleContinueBtn();
	//this.attr_pts = new ReactiveVar(5);

	Meteor.call('getMorals', (error, response) => {
		if (error) console.warn(error.reason);
			if (response) Session.set('morals', response);
	});


})





Template.character_options.events({
	'change select'(event){
		Session.set('attr_pts', 5);
		showCharacterMsg();
		validateCharacterTypeSelected();
		//resetPoints();
	}

})

Template.character_options.helpers({
	'character': function()
	{
		let morals = Session.get('morals');

		return BaseCharacter.find({
			good_evil_flag: {$in: [ morals , "both" ]}
		});
	}
})

Template.attribs.helpers({
	'attribute_pts': function(){
		return Session.get('attr_pts');
	}
})

Template.attribs.events({
	'click .increase_button'(event){
		let button_id = event.currentTarget.id;
		increaseAttributeVal(button_id);

	},
	'click .decrease_button'(event){
		let button_id = event.currentTarget.id;
		decreaseAttributeVal(button_id);

	},

})

Template.create_button.events({
	'click button'(event){

		let current_user = Meteor.userId();
		let user_char = {};

		user_char = sendCreateRequest();

		Meteor.call('insertPlayerChar', user_char, (error, response) => {
			if (error) console.warn(error.reason);
				if (response) console.log(response);
		});

		Meteor.call('initiateWinLoss', function(error, response){
			if (error) console.warn(error.reason);
				if (response) console.log(response);
		});


	  	getCharacterAttacks();




		FlowRouter.go('/location');
	}
})
