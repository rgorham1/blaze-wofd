import { Template } from 'meteor/templating';

import './body.html';


Template.main.events
({

  'click #start'(event)
  {
    event.preventDefault();

    FlowRouter.go('/register');
  },

  'click #continue'(event)
  {
    event.preventDefault();
    FlowRouter.go('/login');
  },

  'click #help'(event)
  {
    event.preventDefault();
    FlowRouter.go('/about');
  },

  'click .logout'(event)
  {
    event.preventDefault();
    Meteor.logout();
  }
});
