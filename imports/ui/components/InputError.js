import React, { Component } from 'react';
import classnames from 'classnames';


export default class InputError extends Component
{
	constructor(props)
	{
		super();
		this.state =
		{
			message: 'Input is invalid'
		};
	}

	render()
	{
		const errorClass = classnames({
			'error_container':   		 true,
      		'visible':           this.props.visible,
      		'invisible':         !this.props.visible
		});

		return (
			<div className={errorClass}>
        	<span>{this.props.errorMessage}</span>
      </div>
			)


	}
}
