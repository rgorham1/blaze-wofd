import React, { Component } from 'react';
import Icon from './Icon';
import SingleInput from './SingleInput';



export default class RegisterScreen extends Component
{
	constructor(props)
	{
		super();
		this.state =
		{
			userName: '',
			email: '',
			password: '',
			confirmPassword: ''
		};



		this.handlePassword = this.handlePassword.bind(this);
		this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
		this.saveAndContinue = this.saveAndContinue.bind(this);
		this.isConfirmedPassword = this.isConfirmedPassword.bind(this);
		this.isEmpty = this.isEmpty.bind(this);
		this.handleUserName = this.handleUserName.bind(this);
		this.handleEmail = this.handleEmail.bind(this);
		this.validateEmail = this.validateEmail.bind(this);
		//this.validateUsername = this.validateUsername.bind(this);

	}


	handlePassword(event)
	{
		if(!_.isEmpty(this.state.confirmPassword))
		{
      		this.refs.passwordConfirm.isValid();
    	}
	    this.refs.passwordConfirm.hideError();
	    this.setState({
	      password: event.target.value
	    });
	}

	handleConfirmPassword(event)
	{
		this.setState({
      		confirmPassword: event.target.value
    	});
	}

	validateEmail(event)
	{
    // regex from http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(event);
  	}
		//TODO: fix validation of user name
		// says user name already taken once any letter is entered in field
		/*
  	validateUsername(event)
  	{
  		Meteor.call('doesUserExist', event, function(error, result)
  		{
  			// `result` is true if the user exists.
  			return result;

		});

  	}
		*/

	handleEmail(event)
	{
    this.setState({
      email: event.target.value
    });
  }

	saveAndContinue(event)
	{
		event.preventDefault();

		let canProceed = this.validateEmail(this.state.email)
	        && this.refs.password.isValid()
	        && this.refs.passwordConfirm.isValid();

	    if(canProceed)
	    {
	      let data = {
	        user: this.state.userName,
			email: this.state.email,
	        password: this.state.password
	      }
				Accounts.createUser({
					email: data['email'],
					password: data['password'],
					username: data['user']
				});
    	}

	    else
	    {
	      this.refs.userName.isValid();
	      this.refs.password.isValid();
	      this.refs.passwordConfirm.isValid();
	    }


	}


	isConfirmedPassword(event)
	{
		return (event == this.state.password)
	}

	handleUserName(event)
	{
		this.setState({
			userName: event.target.value
		})

	}

	isEmpty(value)
	{
		return !_.isEmpty(value);
	}

	render()
	{
		return(
			<div className="create_account_screen">

        		<div className="create_account_form">
		          <form onSubmit={this.saveAndContinue}>

		            <SingleInput
		              text="User Name"
		              ref="username"
		              type="text"
		              defaultValue={this.state.userName}

		              value={this.state.userName}
		              onChange={this.handleUserName}
		              errorMessage="User Name already taken"
		              emptyMessage="User can't be empty"/>

					<SingleInput
			            text="Email Address"
			            ref="email"
			            type="text"
			            defaultValue={this.state.email}
			            validate={this.validateEmail}
			            value={this.state.email}
			            onChange={this.handleEmail}
			            errorMessage="Email address is invalid"
			            emptyMessage="Provide email address"
									errorVisible={this.state.showEmailError}/>


		            <SingleInput
		              text="Password"
		              type="password"
		              ref="password"
		              validator="true"
		              minCharacters="8"
		              requireCapitals="1"
		              requireNumbers="1"
		              value={this.state.passsword}
		              emptyMessage="Password is invalid"
		              onChange={this.handlePassword} />

		            <SingleInput
		              text="Confirm password"
		              ref="passwordConfirm"
		              type="password"
		              validate={this.isConfirmedPassword}
		              value={this.state.confirmPassword}
		              onChange={this.handleConfirmPassword}
		              emptyMessage="Please confirm your password"
		              errorMessage="Passwords don't match"/>

		            <button
		              type="submit"
		              className="button button_wide">
		              Join Warrior Ranks
		            </button>

          		  </form>


        		</div>

      		</div>
		);
	}



}
