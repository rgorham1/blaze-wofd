import React, { Component } from 'react';
import classnames from 'classnames';
import Icon from './Icon';

//const classnames = require('classnames');

export default class PasswordValidator extends Component
{
	constructor(props)
	{
		super(props);
		this.state =
		{
			value: '',
			minCharacters: this.props.minCharacters,
			requireCapitals: this.props.requireCapitals,
			requireNumbers: this.props.requireNumbers,
			name: this.props.name

		};
	}

	render()
	{
		const validatorClass = classnames
		({
      		'password_validator':   true,
      		'visible':              this.props.visible,
      		'invisible':            !this.props.visible
    	});

    	var validatorTitle;

    	if(this.props.valid)
    	{
    		validatorTitle =
    			<h4 className="validator_title valid">
          			{this.props.name} IS OK
        		</h4>
    	}
    	else
    	{
    		validatorTitle =
        		<h4 className="validator_title invalid">
          			{this.props.name} RULES
        		</h4>
    	}


    	return (
    		<div className={validatorClass}>
        		<div className="validator_container">

          			{validatorTitle}

          			<ul className="rules_list">

            			<li className={classnames({'valid': this.props.validData.minChars})}>
			              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
			              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
			              <span className="error_message">{this.state.minCharacters} characters minimum</span>
            			</li>

			            <li className={classnames({'valid': this.props.validData.capitalLetters})}>
			              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
			              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
			              <span className="error_message">Contains at least {this.state.requireCapitals} capital letter</span>
			            </li>

			            <li className={classnames({'valid': this.props.validData.numbers})}>
			              <i className="icon_valid"> <Icon type="circle_tick_filled"/> </i>
			              <i className="icon_invalid"> <Icon type="circle_error"/> </i>
			              <span className="error_message">Contains at least {this.state.requireNumbers} number</span>
			            </li>
          			</ul>
        		</div>
      		</div>
    		)


	}
}
