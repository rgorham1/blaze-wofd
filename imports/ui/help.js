import { Template } from 'meteor/templating';

import './help.html';

Template.about_container.events
({

  'click'(event)
  {
    event.preventDefault();

    FlowRouter.go('/');
  }
});
