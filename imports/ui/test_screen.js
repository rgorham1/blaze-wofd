import { Template } from 'meteor/templating';

import './test_screen.html';

import Morals from '../api/morals/Morals.js';


//TODO: implement session var to track progress so cant keep editing test screen

function moral_calc(morals)
{
  let good = 0;
  let morality;
  for (i in morals)
  {
    if (morals[i] == 'good')
    {
      good++;
    }
  }

  if (good >= 3){
    morality = 'good';
  }
  else{
    morality = 'evil';
  }

  return morality;
}


Template.test.events({
	'click #testInput'(event)
	{
		event.preventDefault();

		let spirit = AutoForm.getFieldValue('spirit_animal','morality_form');
		let honor = AutoForm.getFieldValue('honor','morality_form');
		let humility = AutoForm.getFieldValue('humility','morality_form');
		let diplomacy = AutoForm.getFieldValue('diplomacy','morality_form');
		let kindness = AutoForm.getFieldValue('kindness','morality_form');

		let values = [spirit, kindness, honor, humility, diplomacy];
		morality = moral_calc(values);


		morals = {
			'spirit_animal': spirit,
			'honor': honor,
			'humility': humility,
			'diplomacy': diplomacy,
			'kindness': kindness,
      'morality': morality
		}

    let current_user = Meteor.userId();

    Meteor.users.update(
      current_user,
      { $set: { "profile.morality": morals } }
    );

		FlowRouter.go('/create_character');

	}
})
