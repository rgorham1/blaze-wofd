import { Template } from 'meteor/templating';

import './login.html';

Template.login.events({
  'submit form'(event){
    event.preventDefault();

    let email = $('[name=email]').val();
    let password = $('[name=password]').val();

    Meteor.loginWithPassword(email, password, function(err){
      if(err){
        alert(err.reason);
      }
      else{
        FlowRouter.go('/story');
      }
    });


  }
})
