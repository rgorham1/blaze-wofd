//needed packages
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Random } from 'meteor/random'

//html
import './battle.html';

//needed collections
import Cities from '../api/cities/Cities';
import Playable_Character_Attacks from '../api/playable_character_attacks/Playable_Character_Attacks';
import '../api/base_characters/BaseCharacters';
import NPC_Character_Attacks from '../api/npc_characters/NPC_Character_Attacks';


Template.battle.onRendered(function(){
  let continent = FlowRouter.getParam('stage');
  Session.set('continent',continent.toUpperCase());


  let stage = Session.get('continent');

  Meteor.call('getCurrentUser', (error, response) => {
		if (error) console.warn(error.reason);
			if (response) Session.set('current_user', response);
	});

  Meteor.call('create_playable_character', (error, response) => {
    if (error) console.warn(error.reason);
      if (response) Session.set('player_character', response);
  });


  startInfo = startFight(stage);
  startFightCallback(startInfo);


})



Template.battle.helpers({
  //helper that returns current user's username
  username: function(){
    let user = Session.get('current_user');
    return user.username;
  },
  //helper that returns stage name
  stage: function(){
    return Session.get('continent');
  }


  })


Template.battle.events({
  'click #user_attack_1_name'(event)
  {
    let attack = event.target.innerHTML;
    data = userAttack( attack );
    userAttackCallback(data);

  },

  'click #user_attack_2_name'(event)
  {
    let attack = event.target.innerHTML;
    data = userAttack( attack );
    userAttackCallback(data);

  },

  'click #user_attack_3_name'(event)
  {
    let attack = event.target.innerHTML;
    data = userAttack( attack );
    userAttackCallback(data);

  },
  //TODO: work on this
  // need to fix getting ap back
  'click #skip'(event)
  {
    let data;
    const user = Session.get('current_user');
    let player_character = Session.get('player_character');
    giveBackAp(player_character);
    remainingAp = player_character.AP;
    turn = Session.get('turn');
    let display_txt = `${user.username} decided to take a breather this turn`;
    data =
        {
          return_description: 'skipped',
          display_text: display_txt,
          user_ap: remainingAp,
          user_turn_counter: turn
        };

    userAttackCallback(data);

  },

  'click #summary_button'(event){
    FlowRouter.go('/summary');

  }
})



//TODO: need to work on boss logic and boss stages
//boss location info doesnt seem to be in collections
//picking which boss is supposed to fight
const create_npc = stage =>
{
  let npcName;
  let npcTemplate;
  let npcObject;
  let attackLimit;
  let rand1;
  let rand2;
  let rand3;

  //gathers npc character name from list of possible characters based on stage
  npcName = getEnemyName(stage);

  //TODO: think about adding more templates and maybe tie templates somehow to
  // stage
  //gathers npc template for attributes
  Meteor.call('getNPCTemplates', (error, response) => {
    if (error) console.warn(error.reason);
      if (response) Session.set('npc_template', response);
  });
  npcTemplate = Session.get('npc_template');


  //get attacks information
  attackLimit = NPC_Character_Attacks.find({}).count(); //gets list of possible npc attacks
  //3 random numbers
  rand1 = randomIntFromInterval(1,attackLimit);
  rand2 = randomIntFromInterval(1,attackLimit);
  rand3 = randomIntFromInterval(1,attackLimit);
  //get 3 random attacks using the random numbers
  npcAttacks = NPC_Character_Attacks.find({ attack_id: { $in: [rand1,rand2, rand3] } }).fetch();

  if (npcTemplate)
  {
    npcObject =
    {
      name: npcName,
      type: npcTemplate.character_description,
      HP: npcTemplate.hp,
      AP: npcTemplate.ap,
      maxHP: npcTemplate.hp,
      maxAP: npcTemplate.ap,
      npcAttacks: npcAttacks,
      npcAttributes:
      {
        speed: npcTemplate.speed,
        strength: npcTemplate.strength,
        luck: npcTemplate.luck,
        chi: npcTemplate.chi,
        iq: npcTemplate.intelligence,
        stealth: npcTemplate.stealth
      }

    };
  }
  else console.log('problem with npc creation');

  Session.set('created_npc', npcObject);

}

const getEnemyName = stage =>{
  npcOptions = Cities.find({continent: stage}).fetch(); //get list of possible enemies based on stage

  return Random.choice(npcOptions).character; //chosen character name;
}

//const getNPCTemplate = () =>{
  //let npcTemplates = BaseCharacter.find({npc_flag: true}).fetch();  //get list of npc templates ie fast, lucky, etc.
  //return Random.choice(npcTemplates);
//}


let attackNext;
const startFight = stage =>
{
  let returnObj;
  let npc_type;
  let player_character;
  let npc_character;
  let attackFirst;
  let lucky_text;
  let player_boosted_attr;
  let npc_boosted_attr;
  let player_lucky;
  let npc_lucky;

  const user = Session.get('current_user');

  /*
  Meteor.call('check_npc_type', stage, (error, response) => {
		if (error) console.warn(error.reason);
			if (response) Session.set('npc_type', response);
	});
  npc_type = Session.get('npc_type');
  */

  enableSummaryButton(false);


  player_character = Session.get('player_character');


  create_npc(stage);
  //console.log(`client side npc at start is ${npc_character.name}`);
  npc_character = Session.get('created_npc');


  //TODO: check the geography_user_status table to determine how many attribute points to give to the npc
	//if they are on the first enemy then that npc will get enough attribute points to be 2 levels worse than the user character
	//if they are the second enemy then that npc will get enough attribute points to be 1 level worse than the user character
	//if they are the third or greater than they will be the same level as the user character

  if (player_character)
  {
    player_lucky = AttributeLogic(player_character.playerAttributes.luck);
    if (player_lucky)
    {
      player_boosted_attr = LuckSuccess( player_character);
      let lowercase_player_boosted_attr = player_boosted_attr.toLowerCase();
      //TODO: fix this section with listing boosted attr number
      console.log(player_character.playerAttributes);
      lucky_text = `${user.username} is very lucky! ${player_boosted_attr.toUpperCase()} boosted to ${player_character.playerAttributes.lowercase_player_boosted_attr}!`
    }
    if (npc_character)
    {
      npc_lucky = AttributeLogic(npc_character.npcAttributes.luck);
      if (npc_lucky) {
        npc_boosted_attr = LuckSuccess( npc_character);
        lucky_text = `${npc_character.name} is very lucky! ${npc_boosted_attr.toUpperCase()} boosted!`
      }
    }
    if (player_lucky && npc_lucky) lucky_text = `Both ${user.username} and ${npc_character.name} got a boost!!`;
  }


  attackFirst = whoAttackFirst( player_character, npc_character);

  returnObj =
  {
    display_text: 'the fight has begun',
    luck_text: lucky_text,
    attack_first: attackFirst,
    user_name: user.username,
    npc_name: npc_character.name,
    user_hp: player_character.HP,
    user_ap: player_character.AP,
    npc_hp: npc_character.HP,
    npc_ap: npc_character.AP,
    user_attack1_name: player_character.playerAttacks[0].name,
    user_attack2_name: player_character.playerAttacks[1].name,
    user_attack3_name: player_character.playerAttacks[2].name,
    user_attack1_ap: player_character.playerAttacks[0].cost,
    user_attack2_ap: player_character.playerAttacks[1].cost,
    user_attack3_ap: player_character.playerAttacks[2].cost,
    user_attack1_dmg: player_character.playerAttacks[0].damage,
    user_attack2_dmg: player_character.playerAttacks[1].damage,
    user_attack3_dmg: player_character.playerAttacks[2].damage,
    npc_type_desc: npc_character.type.toLowerCase(),
    user_type_desc: player_character.char_name.toLowerCase()
  };

  return returnObj;


}

//compare stealth attribute to determine if the player or npc attacks first
//TODO: think about this (whether stealth or speed)
const whoAttackFirst = (player, npc) =>
{
  let attackFirst;
  let current_user = Session.get('current_user');

  //console.log(`npc name in attackfirst is ${npc.name}`)
  if (npc)
  {
    if (player.playerAttributes.speed >= npc.npcAttributes.speed)
      attackFirst = current_user.username;
    else
      attackFirst = npc.name;
  }


  return attackFirst;

}



const startFightCallback = data =>
{

	//display text in the text scroll
  //if (data.luck_text) alert(data.luck_text);

	var textConsole = document.getElementById("battle_text");
	textConsole.innerHTML = `<li>${data.display_text}</li>`;
  if (data.luck_text) textConsole.innerHTML = `<li>${data.luck_text}</li>`;
	textConsole.innerHTML += `<li>${data.attack_first} attacks first!</li>`;

	//show the images avatar images
	var userImg = document.getElementById("player_position");
	userImg.innerHTML = "<img src='/img/avatars/"+data.user_type_desc+".jpg' />";
  //console.log(userImg.innerHTML);

	var enemyImg = document.getElementById("enemy_position");
	enemyImg.innerHTML = "<img src='/img/avatars/"+data.npc_type_desc+".jpg' />";




	//display user name where needed
	document.getElementById("disp_player_name_stat").innerHTML = data.user_name+"'s stats";
	document.getElementById("disp_player_name_inv").innerHTML = data.user_name+"'s inventory";

	//display npc name where needed
	document.getElementById("disp_npc_name_stat").innerHTML = data.npc_name+"'s stats";

	//display npc HP
	document.getElementById("display_npc_hp").innerHTML = data.npc_hp;
	document.getElementById("display_npc_ap").innerHTML = data.npc_ap;

	//display user AP
	document.getElementById("display_user_hp").innerHTML = data.user_hp;
	document.getElementById("display_user_ap").innerHTML = data.user_ap;

	//display user Attacks
	document.getElementById("user_attack_1_name").innerHTML = data.user_attack1_name;
	document.getElementById("user_attack_2_name").innerHTML = data.user_attack2_name;
	document.getElementById("user_attack_3_name").innerHTML = data.user_attack3_name;

	//document.getElementById("user_attack_1_id").value = returnObj.user_attack1_id;
	//document.getElementById("user_attack_2_id").value = returnObj.user_attack2_id;
	//document.getElementById("user_attack_3_id").value = returnObj.user_attack3_id;

	document.getElementById("user_attack_1_ap").innerHTML = data.user_attack1_ap+" AP";
	document.getElementById("user_attack_2_ap").innerHTML = data.user_attack2_ap+" AP";
	document.getElementById("user_attack_3_ap").innerHTML = data.user_attack3_ap+" AP";

  //console.log(data);
	determineWhoCanAttack(data);

}

const flashRedUser = () =>
{

	setTimeout(function(){$("#enemy_position").css("background-color", "yellow")},0);
	setTimeout(function(){$("#enemy_position").css("background-color", "red")},100);
	setTimeout(function(){$("#enemy_position").css("background-color", "yellow")},200);
	setTimeout(function(){$("#enemy_position").css("background-color", "red")},300);
	setTimeout(function(){$("#enemy_position").css("background-color", "yellow")},400);
	setTimeout(function(){$("#enemy_position").css("background-color", "red")},500);
	setTimeout(function(){$("#enemy_position").css("background-color", "white")},600);
}


//TODO: add a 'skip'( take a breather) option for times when dont have ap
// if lowest ap move < currentap, skip turn
// or dont use ap

//attack section
// logic for attacking

//logic for user attacking
const userAttack = attackToUse =>
{
  var attack = Playable_Character_Attacks.find( {attack_name: attackToUse }).fetch()[0];
  let returnObj;
  let turn;
  let lucky_turn;
  let lucky_text;
  let wins;
  const user = Session.get('current_user');

  Meteor.call('getWins', function(error, response){
    Session.set('stage_wins', response);
  });

  wins = Session.get('stage_wins');

  Meteor.call('userAttackAction', (error, response) => {
		if (error) console.warn(error.reason);
			if (response) console.log(response);
	});

  Meteor.call('getTurn', (error, response) => {
		if (error) console.warn(error.reason);
			if (response) Session.set('turn', response);
	});

  Meteor.call('checkLuckyTurn', (error, response) => {
		if (error) console.warn(error.reason);
			if (response) Session.set('lucky_turn', response);
	});

  lucky_turn = Session.get('lucky_turn');


  //TODO: figure out why turn isnt working right
  //turn session carries over from last fight for 1st turn
  turn = Session.get('turn');
  if (!turn) turn = 1
  else turn +=1;


  let player_character = Session.get('player_character');
  let npc_character = Session.get('created_npc');

  if (lucky_turn) {
    console.log('lucky');
    let player_lucky = AttributeLogic(player_character.playerAttributes.luck);
    if (player_lucky) {
      player_boosted_attr = LuckSuccess( player_character);
      lucky_text = `${user.username} is very lucky! ${player_boosted_attr.toUpperCase()} boosted!`
      }

  }



  //hit or miss
  let character_speed = player_character.playerAttributes.speed;
  let npc_speed = npc_character.npcAttributes.speed;
  let attackHit = HitSuccess(character_speed, npc_speed);

  //strength affects - heavy or blocked
  let character_strength = player_character.playerAttributes.strength;
  let npc_strength = npc_character.npcAttributes.strength;
  let strengthStrike = AttributeLogic(character_strength);
  //console.log(strengthStrike);


  // critical strike logic
  character_stealth = player_character.playerAttributes.stealth;
  let criticalStrike = AttributeLogic(character_stealth);
  //end crictical strike logic

  let adjustedAttackDamage = attack.damage;
  if (attackHit)
  {
    //check if the player character has enough AP to use the desired move
    if (meetsApRequirement( attack.ap_cost, player_character))
    {
      //set the updated AP
      deductAp(attack.ap_cost, player_character)

      //set the updated HP
      if (criticalStrike) adjustedAttackDamage = CriticalStrikeDamage(attack);
      else if (strengthStrike) adjustedAttackDamage = StrengthDamage(character_strength, npc_strength, attack);
      else if (strengthStrike && criticalStrike) adjustedAttackDamage = CriticalStrikeDamage(attack) + StrengthDamage(character_strength, npc_strength, attack);
      //console.log(adjustedAttackDamage);
      takeHit(adjustedAttackDamage, npc_character)

      let remainingAp = player_character.AP;
      //check if the player character is still alive
    	if(npc_character.HP > 0)
    	{
    		remainingHp = npc_character.HP;

    		display_txt = `${user.username} landed ${attack.attack_name}! <li>The hit does ${adjustedAttackDamage} damage to ${npc_character.name}.</li>`;
        if (criticalStrike)
    		{
    			critical_line = `<li>${user.username} landed a critical hit! ${npc_character.name} is cowering!</li>`;
    			display_txt = display_txt.concat(critical_line);
    		}
    		giveBackAp(player_character);

    		remainingAp = player_character.AP;
    		returnObj =
    		{
    			return_description: 'success',
    			display_text: display_txt,
    			npc_hp: remainingHp,
          npc_name: npc_character.name,
    			user_ap: remainingAp,
    			user_turn_counter: turn
    		};

        returnObj.luck_text = lucky_text;
        //console.log(returnObj);
    	}
      else
      {
        remainingHp = 0;
        display_txt = `${user.username} delivered the finishing blow! You won in ${turn} turns!!`;

        console.log(wins);
        wins++;
        console.log(wins);

        let player = user.username;
        Session.set('winner', player);

        Meteor.call('updateWins', wins, (error, response) => {
      		if (error) console.warn(error.reason);
      			if (response) console.log(response);
      	});

        Meteor.call('winHistory', npc_character.name, (error, response) => {
          if (error) console.warn(error.reason);
            if (response) console.log(response);
        });

        returnObj = {
        	return_description: 'fight_over',
      		display_text: display_txt,
      		npc_hp: remainingHp,
      		user_ap: remainingAp,
      		user_turn_counter: turn

        };

        resetCharacter(npc_character);
        Meteor.call('resetTurns', (error, response) => {
          if (error) console.warn(error.reason);
            if (response) console.log(response);
        });

        enableSummaryButton(true);
        enableSkip(false);
        disableUserAttackOptions();



      }


    }

    else
    {
      giveBackAp(player_character);
      returnObj =
      {
        return_description: 'insufficient_ap',
  		  user_current_ap: player_character.AP,
        npc_name: npc_character.name,
  		  display_text: `${user.username} doesnt have sufficient AP to perform ${attack.attack_name}!`

      };
      returnObj.luck_text = lucky_text;
    }
  }
  else {
    deductAp(attack.ap_cost, player_character);

    let remainingAp = player_character.AP;
    giveBackAp(player_character);
    returnObj =
    {
      return_description: 'Missed',
      user_current_ap: remainingAp,
      npc_name: npc_character.name,
      display_text: `${user.username} missed ${npc_character.name}!`

    };

  }


  Session.set('created_npc', npc_character);
  Session.set('player_character', player_character);
  return returnObj;



};


const npcAttack = () =>
{
  let player_character = Session.get('player_character');
  let npc_character = Session.get('created_npc');

  const user = Session.get('current_user');

  let returnObj;
  let turn;
  let losses;
  let npcName;
  let attack;

  let character_speed;
  let npc_speed;

  turn = Session.get('turn');

  Meteor.call('getLosses', function(error, result){
    Session.set('stage_losses', result);
    });

  losses = Session.get('stage_losses');


  npcName = npc_character.name;
  enableSkip(false);

  attack = chooseAttack(npc_character);



  //hit or miss
  character_speed = player_character.playerAttributes.speed;
  npc_speed = npc_character.npcAttributes.speed;
  let attackHit = HitSuccess(npc_speed, character_speed);

  //strength affects - heavy or blocked
  let character_strength = player_character.playerAttributes.strength;
  let npc_strength = npc_character.npcAttributes.strength;
  let strengthStrike = AttributeLogic(npc_strength);

  //critical strike logic
  let npcStealth = npc_character.npcAttributes.stealth;
  let criticalStrike = AttributeLogic(npcStealth);

  let adjustedAttackDamage = attack.damage;
  if (attackHit){
    if (meetsApRequirement( attack.ap_cost, npc_character))
    {
    	//set the updated AP
      //console.log(attack.ap_cost, npc_character.AP);
      deductAp(attack.ap_cost, npc_character);

      //set the updated HP
      if (criticalStrike) adjustedAttackDamage = CriticalStrikeDamage(attack);
      else if (strengthStrike) adjustedAttackDamage = StrengthDamage(npc_strength, character_strength, attack);
      else if (strengthStrike && criticalStrike) adjustedAttackDamage = CriticalStrikeDamage(attack) + StrengthDamage(npc_strength, character_strength, attack);
      takeHit(adjustedAttackDamage, player_character)

      let remainingAp = npc_character.AP;

      //check if the player character is still alive
    	if(player_character.HP > 0)
    	{

    		remainingHp = player_character.HP;

    		display_txt = `${npcName} used ${attack.attack_name}!`;
    		if (criticalStrike)
    		{
    			critical_line = `<li>${npcName} landed a critical hit! ${user.username} is trying not to cry!</li>`;
    			display_txt = display_txt.concat(critical_line);
    		}

    		display_txt_cont = `<li>${attack.attack_name} hits ${user.username} for ${adjustedAttackDamage}.</li>`;
    		display_txt = display_txt.concat(display_txt_cont);

    		giveBackAp(npc_character);

    		remainingAp = npc_character.AP;
        //console.log(remainingAp);
    		returnObj =
    		{
    			return_description: 'success',
    			user_name: user.username,
    			display_text: display_txt,
    			npc_name: npcName,
    			user_hp: remainingHp,
    			npc_ap: remainingAp,
    			user_turn_counter: turn
    		};


    	}

    	else
    	{
    		remainingHp = 0;
    		display_txt = `<li>${npcName} delivered the finishing blow!</li> <li>You lost in ${turn} turns - you suck!</li>`;


        losses++;

        let cpu = npcName;
        Session.set('winner', cpu);
        Meteor.call('updateLosses', losses, function(error, response){
            if (error) console.warn(error.reason);
              if (response) console.log(response);
          });

        returnObj =
    		{
    			return_description: 'fight_over',
    			display_text: display_txt,
    			npc_name: npcName,
    			user_hp: remainingHp,
    			npc_ap: remainingAp,
    			user_turn_counter: turn
    		};

        resetCharacter(player_character);

        enableSummaryButton(true);


    	}

    }

    else
    {
    	giveBackAp(npc_character);
    	returnObj =
  		{
  			return_description: 'insufficient_ap',
  			npc_current_ap: npc_character.AP,
  			display_text: `${npcName} doesnt have sufficient AP to perform ${attack.attack_name}!`,
  			user_name: player_character.char_name,
  			npc_name: npcName
  		};


    }
  }

  else
  {
    //TODO: think about this
    //whether still deduct AP for missed attacks
    deductAp(attack.ap_cost, npc_character);

    let remainingAp = npc_character.AP;
    giveBackAp(npc_character);
    returnObj =
    {
      return_description: 'Missed',
      npc_current_ap: remainingAp,
      user_name: player_character.char_name,
      display_text: `${npcName} missed ${user.username}! Whew!!`

    };

  }

  Session.set('created_npc', npc_character);
  Session.set('player_character', player_character);


  return returnObj;


}

//TODO: fix this to get info from collection instead in case rework maxHP/AP to
// be higher than initial HP/AP
const resetCharacter = character =>{
  character.HP = character.maxHP;
  character.AP = character.maxAP;
}

//chooses random npc attack based on whether has ap to do move
//if not cycles through attacks to check if has ap to do an attack
const chooseAttack = npc =>{
  let generatedAttackChoice;
  let chosenAttack;
  let move;
  let moves;
  let possible_moves = [];

  moves = npc.npcAttacks;

  for (move in moves){
    if (meetsApRequirement(moves[move].ap_cost, npc))
      possible_moves.push(moves[move]);
  }

  //randomly pick which attack the npc will use
  if (possible_moves){
    generatedAttackChoice = randomIntFromInterval(0,possible_moves.length -1);
    chosenAttack = possible_moves[generatedAttackChoice];
  }
  else{
    generatedAttackChoice = randomIntFromInterval(0,2);
    chosenAttack = moves[generatedAttackChoice];
  }

  //console.log(chosenAttack);

  return chosenAttack;


};

const HitSuccess = (offense_speed, defense_speed) =>{
  let target;
  //character_speed = player_character.playerAttributes.speed;
  let hit = AttributeLogic(offense_speed);
  //npc_speed = npc_character.npcAttributes.luck;
  let miss = AttributeLogic(defense_speed);

  if (hit && miss)
  {
    if (offense_speed > defense_speed) target = true;
    else if (offense_speed < defense_speed) target = false;

    else
    {
      let offense_num = randomIntFromInterval(1, 100);
      let defense_num = randomIntFromInterval(1, 100);
      let hit_num = randomIntFromInterval(1,100);
      if (Math.abs(offense_num - hit_num) < Math.abs(defense_num - hit_num)) target = true;
      else target = false;

    }

  }
  else if (miss) target = false;
  else target = true;

  return target;
}

const AttributeLogic = character_attribute => {

  let logic = false;

  randomRange = randomRangeCalculator(character_attribute);
  let theRange = randomRange[0];
  let rangeMax = randomRange[1];


  let logicNumbers = [];
  let i = 0;
  let number;
  while(logicNumbers.length < theRange)
  {
    number = randomIntFromInterval(1, rangeMax);
    let arrayCheck  = in_array(number, logicNumbers);
    if (arrayCheck == -1)
    {
      logicNumbers[i] = number;
      i++;
    }

  }

  let findThisforHit = randomIntFromInterval(1, rangeMax);

  let logicCheck = in_array(findThisforHit, logicNumbers);
  if (logicCheck >= 0) logic = true;

  return logic;


}

const randomRangeCalculator = character_attr =>{
  let theRange;
  let rangeMax = 20;

  if (character_attr == 1) theRange = 0;
  else if (character_attr ==2)
  {
    theRange = 1;
    rangeMax--;
  }
  else if (character_attr >= 3 && character_attr <= 5)
  {
    theRange = 2;
    rangeMax--;
    if (character_attr == 4) rangeMax--;
    else if (character_attr == 5) rangeMax = (rangeMax - 2);
  }
  else if (character_attr == 6 || character_attr == 7)
  {
    theRange = 3;
    rangeMax = (rangeMax -3);
    if (character_attr == 7) rangeMax--;
  }
  else if( character_attr == 8 || character_attr == 9)
	{
		theRange = 4;
		rangeMax = (rangeMax - 3);

		if(character_attr == 9) rangeMax = (rangeMax - 2);
  }
	else if( character_attr == 10)
	{
		theRange = 6;
		rangeMax = (rangeMax - 4);
	}
  //account for possible boost from luck
  else if( character_attr > 10)
	{
		theRange = 7;
		rangeMax = (rangeMax - 4);
	}

  return [theRange, rangeMax];

}


const CriticalStrikeDamage = attack => attack.damage +20;


const StrengthDamage = (offense_strength, defense_strength, attack) =>{
  let strongStrike = AttributeLogic(offense_strength);
  let blockStrike = AttributeLogic(defense_strength);
  let adjustedAttackDamage = attack.damage;
  let blockedDamage = attack.damage - adjustedDamage;
  let adjustedDamage = Math.ceil(attack.damage * getMultiplier(offense_strength));

  if (strongStrike && blockStrike){
    adjustedAttackDamage = attack.damage + adjustedDamage;
    adjustedAttackDamage = adjustedAttackDamage - blockedDamage;

  }

  else if (strongStrike) adjustedAttackDamage = attack.damage + adjustedDamage;

  else if (blockStrike) adjustedAttackDamage = blockedDamage;

  return adjustedAttackDamage;

}

const LuckSuccess = character =>{
  let character_attrs;
  let lucky_attr;
  let attrs = ['stealth', 'strength', 'speed', 'chi'];
  let random_index = randomIntFromInterval(0, (attrs.length -1));

  let random_attr = attrs[random_index];

  if (!character.playerAttributes)
  		character_attrs = character.npcAttributes;
  	else
  		character_attrs = character.playerAttributes;

  lucky_attr = Number(character_attrs[random_attr]);
  lucky_attr += 2;
  character_attrs[random_attr] = lucky_attr;

  return random_attr;

}

//returns random number between min and max
const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min);

const in_array = (number, arrayToCheck) => arrayToCheck.indexOf(number);

const getMultiplier = attr =>
{
	multiplier = 0;

	if(attr == 1)
		multiplier = 0.1;
	else if(attr == 2)
		multiplier = 0.2;
	else if(attr == 3)
		multiplier = 0.3;
	else if(attr == 4)
		multiplier = 0.4;
	else if(attr == 5)
		multiplier = 0.5;
	else if(attr == 6)
		multiplier = 0.6;
	else if(attr == 7)
		multiplier = 0.7;
	else if(attr == 8)
		multiplier = 0.8;
	else if(attr == 9)
		multiplier = 0.9;
	else if(attr == 10)
		multiplier = 1;
  else if(attr > 10)
  	multiplier = 1.1;

	return multiplier;
}

const meetsApRequirement = (moveApCost, character) =>
{
  //console.log(moveApCost, character.AP);
	if(moveApCost <= character.AP)
		return true;

	return false;
}

const deductAp = (apPoints, character) =>
{
	newAp = (character.AP - apPoints);
	if(newAp <= 0)
		character.AP = 0;
	else
		character.AP = newAp;
}

const takeHit = (dmg, character) =>
{
	hp = character.HP - dmg;

	if(hp <= 0)
		character.HP = 0;
	else
		character.HP = hp;
}


const giveBackAp = character =>
{
	let multiplier;
	let chiAttribute;
  let numOfAp;

	if (!character.playerAttributes)
  		chiAttribute = character.npcAttributes.chi;
  	else
  		chiAttribute = character.playerAttributes.chi;

  multiplier = getMultiplier(chiAttribute);

	numOfAp = Math.ceil((character.maxAP * multiplier) /5);  //round up to the nearest integer


	if((character.AP + numOfAp) > character.maxAP)
		character.AP = character.maxAP;
	else
		character.AP = (character.AP + numOfAp);
}


const userAttackCallback = data  =>
{
	//console.log(`user attack callback data is ${data}`);
	var textConsole = document.getElementById("battle_text");

	if(data.return_description == "success")
	{
		flashRedUser();
		//update the message in the console window
		textConsole.innerHTML = "<li>"+data.display_text+"</li>";

		//update npc HP
		document.getElementById("display_npc_hp").innerHTML = data.npc_hp;

		//updated user AP
		document.getElementById("display_user_ap").innerHTML = data.user_ap;

		//set the variable that determines who attacks next to the the npc
		attackNext = data.npc_name;
		determineWhoCanAttack(data);
	}
	else if(data.return_description == "insufficient_ap")
	{
		//update the message in the console window to let the user know they do not have enough AP to use their attack
		attackNext = data.npc_name;
		document.getElementById('display_user_ap').innerHTML = data.user_current_ap;
		textConsole.innerHTML = "<li>"+data.display_text+"</li><li>Skipping turn..</li>";
		determineWhoCanAttack(data);
	}
	else if(data.return_description == "fight_over")
	{
		//update the message in the console window to let the know they have won the fight
		textConsole.innerHTML = "<li>"+data.display_text+"</li>";

		//update npc HP
		document.getElementById("display_npc_hp").innerHTML = data.npc_hp;

		//update user character AP
		document.getElementById("display_user_ap").innerHTML = data.user_ap;

		//disableUserFromAttacking();

		//setTimeout(function(){redirectToLocations()},500);
	}
  else if (data.return_description == "Missed") {
    attackNext = data.npc_name;
		document.getElementById('display_user_ap').innerHTML = data.user_current_ap;
		textConsole.innerHTML = "<li>"+data.display_text+"</li>";
		determineWhoCanAttack(data);

  }
  else if (data.return_description == "skipped") {
    attackNext = data.npc_name;
    document.getElementById("display_user_ap").innerHTML = data.user_ap;
    textConsole.innerHTML = "<li>"+data.display_text+"</li>";
    determineWhoCanAttack(data);

  }
	else
	{
		textConsole.innerHTML = data;
	}
}

function redirectToLocations()
{
	window.location.assign("choose_location.php");
}
const flashRedNpc = () =>
{
	setTimeout(function(){$("#player_position").css("background-color", "yellow")},0);
	setTimeout(function(){$("#player_position").css("background-color", "red")},100);
	setTimeout(function(){$("#player_position").css("background-color", "yellow")},200);
	setTimeout(function(){$("#player_position").css("background-color", "red")},300);
	setTimeout(function(){$("#player_position").css("background-color", "yellow")},400);
	setTimeout(function(){$("#player_position").css("background-color", "red")},500);
	setTimeout(function(){$("#player_position").css("background-color", "white")},600);
}


const npcAttackCallback = () =>
{
  let data = npcAttack();
	//console.log(data);
	var textConsole = document.getElementById("battle_text");


	if(data.return_description == "success")
	{
		flashRedNpc();
		//update the message in the console window
		textConsole.innerHTML = data.display_text;

		//update user HP
		document.getElementById("display_user_hp").innerHTML = data.user_hp;

		//update npc AP
		document.getElementById("display_npc_ap").innerHTML = data.npc_ap;
    enableSkip(true);

		//set the variable that determines who attacks next to the the user character
		attackNext = data.user_name;
		determineWhoCanAttack(data);
	}
	else if(data.return_description == "insufficient_ap")
	{
		//update the message in the console window to let the user know the npc does not have enough AP to use their attack
		textConsole.innerHTML = "<li>"+data.display_text+"</li><li>Skipping turn..</li>";
		document.getElementById('display_npc_ap').innerHTML = data.npc_current_ap;
		attackNext = data.user_name;
		determineWhoCanAttack(data);
	}
  else if (data.return_description == "Missed") {
    attackNext = data.user_name;
    document.getElementById('display_npc_ap').innerHTML = data.npc_current_ap;
    textConsole.innerHTML = "<li>"+data.display_text+"</li>";
    determineWhoCanAttack(data);
    enableSkip(false);

  }
	else if(data.return_description == "fight_over")
	{
		//update the message in the console window to let the know they have lost the fight
		textConsole.innerHTML = data.display_text;

		//update user HP
		document.getElementById("display_user_hp").innerHTML = data.user_hp;

		//update npc AP
		document.getElementById("display_npc_ap").innerHTML = data.npc_ap;

		//the fight is over so the user shouldnt be allowed to try to attack anymore
		disableUserAttackOptions();

		//exit the fight view and direct the user to the fight summary screen
		//setTimeout(function(){redirectToLocations()},500);
	}
	else
	{
		textConsole.innerHTML = data;
	}
}


//function directToSummaryPage( theReturnObject )



//miscellaneous functions
const disableUserAttackOptions = () =>
{
	$("#user_attack_1_name").prop("disabled",true);
	$("#user_attack_2_name").prop("disabled",true);
	$("#user_attack_3_name").prop("disabled",true);

}

const enableSkip = flag =>{
  if (flag === true)
    $('#skip').prop('disabled', false);
  else {
    $('#skip').prop('disabled', true);
  }
}

const enableSummaryButton = flag =>
{
  if (flag === true)
    $('#summary_button').prop('disabled', false);
  else
    $('#summary_button').prop('disabled', true);
}

const enableUserAttackOptions = () =>
{

	$("#user_attack_1_name").prop("disabled", false);
	$("#user_attack_2_name").prop("disabled", false);
	$("#user_attack_3_name").prop("disabled", false);

}

const determineWhoCanAttack = returnData =>
{
  //console.log(`attackNext at point is ${attackNext}`);
  //console.log(returnData);
	if(!attackNext)
	{
		disableUserAttackOptions();
		//console.log(returnData.attack_first);
		//console.log(`npc name is ${returnData.npc_name}`);
		if(returnData.attack_first == returnData.npc_name){
			setTimeout(npcAttackCallback, 4000);
      //console.log('npc attack first');

			//npcAttackCallback();

		}


		else if(returnData.attack_first == returnData.user_name)
			setTimeout(enableUserAttackOptions, 3000);
	}
	else if(attackNext == returnData.npc_name)
	{
		disableUserAttackOptions();
		setTimeout(npcAttackCallback, 2500);
		//npcAttackCallback();

	}
	else
	{
		setTimeout(enableUserAttackOptions, 1000); //wait a second to let the user execute another attack
	}
}
