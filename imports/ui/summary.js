import { Template } from 'meteor/templating';

import './summary.html';

Template.summary.helpers({
  winner: function(){
    let winner =Session.get('winner');
    return winner
  },

  turns: function(){
    let turn = Session.get('turn');
    if (!turn) turn = 1;
    return turn;
  }
})

Template.summary.events({
  'click button'(event){
    FlowRouter.go('/location');
  }
})
