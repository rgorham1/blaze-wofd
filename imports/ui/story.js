import { Template } from 'meteor/templating';

import './story.html';

Template.story.events({
  'click #storybutton'(event)
  {
    event.preventDefault();
    FlowRouter.go('/test');
  }
})
