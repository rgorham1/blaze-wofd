FlowRouter.route( '/', {
  action: function() {
    // Do whatever we need to do when we visit http://app.com/register
    BlazeLayout.render("mainLayout", {content: "main"});
  },
  name: 'home' // Optional route name.
});


FlowRouter.route( '/register', {
  action: function() {
    // Do whatever we need to do when we visit http://app.com/register
    BlazeLayout.render("mainLayout", {content: "register_screen"});
  },
  name: 'registration' // Optional route name.
});

FlowRouter.route( '/about', {
  action: function() {
    // Do whatever we need to do when we visit http://app.com/register
    BlazeLayout.render("mainLayout", {content: "about_screen"});
  },
  name: 'help' // Optional route name.
});

FlowRouter.route( '/login', {
  action: function() {
    // Do whatever we need to do when we visit http://app.com/register
    BlazeLayout.render("mainLayout", {content: "login"});
  },
  name: 'continue' // Optional route name.
});

FlowRouter.route( '/story', {
  action: function() {
    // Do whatever we need to do when we visit http://app.com/register
    BlazeLayout.render("mainLayout", {content: "story"});
  },
  name: 'story' // Optional route name.
});

FlowRouter.route( '/test', {
  action: function() {
    BlazeLayout.render("mainLayout", {content: "test"});
  },
  name: 'test' // Optional route name.
});

FlowRouter.route( '/create_character', {
  action: function() {
    BlazeLayout.render("mainLayout", {content: "create_character"});
  },
  name: 'create_character' // Optional route name.
});

FlowRouter.route( '/location', {
  action: function() {
    BlazeLayout.render("mainLayout", {content: "stages"});
  },
  name: 'location' // Optional route name.
});


FlowRouter.route( '/battle/:stage', {
  action: function(params) {
    BlazeLayout.render("mainLayout", {content: "battle"});
    //console.log(params.stage);
  },
  name: 'battle' // Optional route name.
});

FlowRouter.route( '/summary', {
  action: function() {
    BlazeLayout.render("mainLayout", {content: "summary"});
  },
  name: 'summary' // Optional route name.
});





/*
let battleSection = FlowRouter.group({
  prefix: '/battle'
});

battleSection.route('/', {
  action: function(){
    console.log('battle page');
  }
});

battleSection.route('/africa', {
  action: function(){
    console.log('africa battle page');
  },
  name: 'Africa Stage'
});

battleSection.route('/australia', {
  action: function(){
    console.log('australia battle page');
  }
});

battleSection.route('/europe', {
  action: function(){
    console.log('australia battle page');
  },

  name: 'Europe stage'
});

battleSection.route('/asia', {
  action: function(){
    console.log('australia battle page');
  }
});

battleSection.route('/antartica', {
  action: function(){
    console.log('australia battle page');
  }
});

battleSection.route('/north_america', {
  action: function(){
    console.log('australia battle page');
  }
});

battleSection.route('/south_america', {
  action: function(){
    console.log('australia battle page');
  }
});
*/
