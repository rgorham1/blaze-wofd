import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';


const NPC_Characters = new Mongo.Collection('npc_characters');

NpcCharactersSchema = new SimpleSchema({
	character_name:
	{
		type: String
	},

	city:
	{
		type: String
	}
});

NPC_Characters.schema = NpcCharactersSchema;

NPC_Characters.allow({
  insert: () => true,
  update: () => false,
  remove: () => false
});

NPC_Characters.deny({
  insert: () => false,
  update: () => true,
  remove: () => true
});

var npc_characters = [
  {
    "character_name": "Tuco",
    "city": "Caritoba"
  },
  {
    "character_name": "Dr. Frostman",
    "city": "Research Station Alpha"
  },
  {
    "character_name": "Ice Guy",
    "city": "Frozen Rock"
  },
  {
    "character_name": "Mr. Fubu",
    "city": "Israel"
  },
  {
    "character_name": "Caesar",
    "city": "Chinatown"
  },
  {
    "character_name": "Lion Spear",
    "city": "Beijing"
  },
  {
    "character_name": "Moshing Sandman",
    "city": "Hong Kong"
  },
  {
    "character_name": "Dancing Tortuga",
    "city": "Chicago"
  },
  {
    "character_name": "Grace",
    "city": "Boston"
  },
  {
    "character_name": "Smoking Iguana",
    "city": "NC"
  },
  {
    "character_name": "Chief Mountaintop",
    "city": "West VA"
  },
  {
    "character_name": "Shawn \"Cold Stone\" Johnson",
    "city": "Paris"
  },
  {
    "character_name": "25",
    "city": "London"
  },
  {
    "character_name": "One Eyed Pete",
    "city": "Switzerland"
  },
  {
    "character_name": "Kool Jimmy",
    "city": "Norway"
  },
  {
    "character_name": "General Clay",
    "city": "NYC"
  },
  {
    "character_name": "Lemon Peel",
    "city": "Siberia"
  },
  {
    "character_name": "Dudkoff",
    "city": "Alcatraz"
  },
  {
    "character_name": "Canadian Punisher",
    "city": "Scotland"
  },
  {
    "character_name": "Ben Fostershire",
    "city": "Peter Island"
  },
  {
    "character_name": "Demon of Tazmania",
    "city": "New Zealand"
  },
  {
    "character_name": "Sean Paul Dame Van",
    "city": "South Africa"
  },
  {
    "character_name": "Pierre G.",
    "city": "Congo"
  },
  {
    "character_name": "Erik Thunderborn",
    "city": "Zimbabwe"
  },
  {
    "character_name": "Cold Feather",
    "city": "Nigeria"
  },
  {
    "character_name": "Twinkletoes",
    "city": "Liberia"
  },
  {
    "character_name": "Lord Clegane",
    "city": "Belgium"
  },
  {
    "character_name": "Pony Tailed Fat Seagull",
    "city": "India"
  },
  {
    "character_name": "Chocolate",
    "city": "Tasmania"
  },
  {
    "character_name": "Sandstorm",
    "city": "Malaysia"
  },
  {
    "character_name": "Ghost Faced Chef",
    "city": "Mt. Fuji"
  },
  {
    "character_name": "Long-Legged Killer",
    "city": "Sydney"
  },
  {
    "character_name": "Prince Xianche",
    "city": "Tokyo"
  },
  {
    "character_name": "Eatsan",
    "city": "Shanghai"
  },
  {
    "character_name": "Jet Chan",
    "city": "Temple"
  },
  {
    "character_name": "Praying Monster",
    "city": "Okinawa"
  },
  {
    "character_name": "Donnie Yeoh",
    "city": "Korea"
  },
  {
    "character_name": "Paz",
    "city": "Venezuela"
  },
  {
    "character_name": "Ace",
    "city": "Argentina"
  },
  {
    "character_name": "Vega",
    "city": "Lima"
  },
  {
    "character_name": "Prieto",
    "city": "Rio de Janeiro"
  }
];

export default NPC_Characters;
/*
npc_characters.forEach(doc =>{
	NPC_Characters.insert(doc);
});
*/
