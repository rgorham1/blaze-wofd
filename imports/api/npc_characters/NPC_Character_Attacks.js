import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';



const NPC_Character_Attacks = new Mongo.Collection('npc_character_attacks');


NpcCharacterAttacksSchema = new SimpleSchema({
	attack_id:
	{
		type: Number
	},

	attack_name:
	{
		type: String
	},

	damage:
	{
		type: Number
	},

	ap_cost:
	{
		type: Number
	},

	unique:
	{
		type: Boolean
	},

	city:
	{
		type: String,
		optional: true
	}

});

NPC_Character_Attacks.schema = NpcCharacterAttacksSchema;

var npc_character_attack = [
  {
    "attack_id": 1,
    "attack_name": "Cold As Ice",
    "damage": 32,
    "ap_cost": 30,
    "unique": true,
    "city": "Frozen Rock"
  },
  {
    "attack_id": 2,
    "attack_name": "Strong Isiquili",
    "damage": 35,
    "ap_cost": 35,
    "unique": true,
    "city": "South Africa"
  },
  {
    "attack_id": 3,
    "attack_name": "Gorilla Fist",
    "damage": 20,
    "ap_cost": 15,
    "unique": true,
    "city": "Congo"
  },
  {
    "attack_id": 4,
    "attack_name": "Glass Covered Fist",
    "damage": 28,
    "ap_cost": 27,
    "unique": true,
    "city": "Zimbabwe"
  },
  {
    "attack_id": 5,
    "attack_name": "Rooster''s Crow",
    "damage": 22,
    "ap_cost": 20,
    "unique": true,
    "city": "Nigeria"
  },
  {
    "attack_id": 6,
    "attack_name": "Break Kick",
    "damage": "26",
    "ap_cost": "25",
    "unique": true,
    "city": "Argentina"
  },
  {
    "attack_id": 7,
    "attack_name": "Stone Hands",
    "damage": 29,
    "ap_cost": 30,
    "unique": true,
    "city": "Lima"
  },
  {
    "attack_id": 8,
    "attack_name": "Deceptive Strikes",
    "damage": 21,
    "ap_cost": 20,
    "unique": true,
    "city": "Venezuela"
  },
  {
    "attack_id": 9,
    "attack_name": "Neck Breaker",
    "damage": 25,
    "ap_cost": 25,
    "unique": true,
    "city": "Caritoba"
  },
  {
    "attack_id": 10,
    "attack_name": "Jumping Piledriver",
    "damage": 34,
    "ap_cost": 35,
    "unique": true,
    "city": "NYC"
  },
  {
    "attack_id": 11,
    "attack_name": "104 Hand Blocks",
    "damage": 30,
    "ap_cost": 30,
    "unique": true,
    "city": "Alcatraz"
  },
  {
    "attack_id": 12,
    "attack_name": "Cyclops",
    "damage": 32,
    "ap_cost": 30,
    "unique": true,
    "city": "West VA"
  },
  {
    "attack_id": 13,
    "attack_name": "Drink the KoolAid",
    "damage": 21,
    "ap_cost": 17,
    "unique": true,
    "city": "Boston"
  },
  {
    "attack_id": 14,
    "attack_name": "Whippersnapper",
    "damage": 20,
    "ap_cost": 18,
    "unique": true,
    "city": "Las Vegas"
  },
  {
    "attack_id": 15,
    "attack_name": "One Hitter Quitter",
    "damage": 35,
    "ap_cost": 33,
    "unique": true,
    "city": "Chicago"
  },
  {
    "attack_id": 16,
    "attack_name": "3 Ninjas",
    "damage": 34,
    "ap_cost": 32,
    "unique": true,
    "city": "LA"
  },
  {
    "attack_id": 17,
    "attack_name": "Grandfather''s Fist",
    "damage": 19,
    "ap_cost": 15,
    "unique": true,
    "city": "Montreal"
  },
  {
    "attack_id": 18,
    "attack_name": "Jumping Nut Buster",
    "damage": 22,
    "ap_cost": 20,
    "unique": true,
    "city": "Sydney"
  },
  {
    "attack_id": 19,
    "attack_name": "Tu Matauenga",
    "damage": 23,
    "ap_cost": 23,
    "unique": true,
    "city": "New Zealand"
  },
  {
    "attack_id": 20,
    "attack_name": "Epic Split Kick",
    "damage": 36,
    "ap_cost": 38,
    "unique": true,
    "city": "Belgium"
  },
  {
    "attack_id": 21,
    "attack_name": "Mirrors Edge",
    "damage": 15,
    "ap_cost": 14,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 22,
    "attack_name": "Double Spinning Back Kick",
    "damage": 25,
    "ap_cost": 25,
    "unique": true,
    "city": "Paris"
  },
  {
    "attack_id": 23,
    "attack_name": "Wrath of Odin",
    "damage": 27,
    "ap_cost": 25,
    "unique": true,
    "city": "Norway"
  },
  {
    "attack_id": 24,
    "attack_name": "Pointy End",
    "damage": 31,
    "ap_cost": 30,
    "unique": true,
    "city": "Switzerland"
  },
  {
    "attack_id": 25,
    "attack_name": "20 Pressure Points",
    "damage": 23,
    "ap_cost": 23,
    "unique": true,
    "city": "Siberia"
  },
  {
    "attack_id": 26,
    "attack_name": "Steel Tipped Boots",
    "damage": 22,
    "ap_cost": 23,
    "unique": true,
    "city": "Scotland"
  },
  {
    "attack_id": 27,
    "attack_name": "Marked for Death",
    "damage": 24,
    "ap_cost": 23,
    "unique": true,
    "city": "Tokyo"
  },
  {
    "attack_id": 28,
    "attack_name": "Rage of Phoenix",
    "damage": 28,
    "ap_cost": 26,
    "unique": true,
    "city": "Korea"
  },
  {
    "attack_id": 29,
    "attack_name": "Drunken Monkey",
    "damage": 29,
    "ap_cost": 27,
    "unique": true,
    "city": "Temple"
  },
  {
    "attack_id": 30,
    "attack_name": "Wind Dance",
    "damage": 27,
    "ap_cost": 26,
    "unique": true,
    "city": "Chinatown"
  },
  {
    "attack_id": 31,
    "attack_name": "Nose Crushing Palm",
    "damage": 24,
    "ap_cost": 25,
    "unique": true,
    "city": "Israel"
  },
  {
    "attack_id": 32,
    "attack_name": "Demon Kris",
    "damage": 30,
    "ap_cost": 29,
    "unique": true,
    "city": "Malaysia"
  },
  {
    "attack_id": 33,
    "attack_name": "Horse Punch",
    "damage": 30,
    "ap_cost": 30,
    "unique": true,
    "city": "Okinawa"
  },
  {
    "attack_id": 34,
    "attack_name": "Swallower",
    "damage": 27,
    "ap_cost": 28,
    "unique": true,
    "city": "Mt. Fuji"
  },
  {
    "attack_id": 35,
    "attack_name": "Ip",
    "damage": 31,
    "ap_cost": 30,
    "unique": true,
    "city": "Shanghai"
  },
  {
    "attack_id": 36,
    "attack_name": "8 armed goddess",
    "damage": 25,
    "ap_cost": 29,
    "unique": true,
    "city": "India"
  },
  {
    "attack_id": 37,
    "attack_name": "Bull rush",
    "damage": 25,
    "ap_cost": 24,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 38,
    "attack_name": "Snake Fist",
    "damage": 27,
    "ap_cost": 25,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 39,
    "attack_name": "Super Knee",
    "damage": 23,
    "ap_cost": 21,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 40,
    "attack_name": "Fist of Light",
    "damage": 22,
    "ap_cost": 21,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 41,
    "attack_name": "The Darkness",
    "damage": 21,
    "ap_cost": 20,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 42,
    "attack_name": "TZA",
    "damage": 27,
    "ap_cost": 25,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 43,
    "attack_name": "Man of Methods",
    "damage": 30,
    "ap_cost": 31,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 44,
    "attack_name": "Iron Chef",
    "damage": 28,
    "ap_cost": 27,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 45,
    "attack_name": "20 Legs from Above",
    "damage": 20,
    "ap_cost": 19,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 46,
    "attack_name": "ShadowWalker",
    "damage": 21,
    "ap_cost": 20,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 47,
    "attack_name": "Valar Morghulis",
    "damage": 31,
    "ap_cost": 33,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 48,
    "attack_name": "3 Headed Dragon",
    "damage": 32,
    "ap_cost": 34,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 49,
    "attack_name": "Dancing Master",
    "damage": 29,
    "ap_cost": 27,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 50,
    "attack_name": "Prickly Needle",
    "damage": 26,
    "ap_cost": 25,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 51,
    "attack_name": "Shadow of the Night",
    "damage": 25,
    "ap_cost": 26,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 52,
    "attack_name": "Beastmaster 12",
    "damage": 23,
    "ap_cost": 21,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 53,
    "attack_name": "Goon Blast",
    "damage": 27,
    "ap_cost": 25,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 54,
    "attack_name": "Rack of Torture",
    "damage": 20,
    "ap_cost": 22,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 55,
    "attack_name": "Bottom of the Rock",
    "damage": 30,
    "ap_cost": 31,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 56,
    "attack_name": "Lovely Cheek Tunes",
    "damage": "31",
    "ap_cost": "32",
    "unique": false,
    "city": null
  },
  {
    "attack_id": 57,
    "attack_name": "Death Valley NailDriver",
    "damage": 33,
    "ap_cost": 35,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 58,
    "attack_name": "Atomic Legs",
    "damage": 20,
    "ap_cost": 20,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 59,
    "attack_name": "Toad Splash",
    "damage": 21,
    "ap_cost": 20,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 60,
    "attack_name": "WigSplitter",
    "damage": 28,
    "ap_cost": 30,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 61,
    "attack_name": "Shonuff",
    "damage": 24,
    "ap_cost": 24,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 62,
    "attack_name": "Devour",
    "damage": 23,
    "ap_cost": 22,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 63,
    "attack_name": "Black Magic Man",
    "damage": 29,
    "ap_cost": 28,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 64,
    "attack_name": "Smooth Deviant",
    "damage": 25,
    "ap_cost": 25,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 65,
    "attack_name": "Bugs Marching",
    "damage": 32,
    "ap_cost": 30,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 66,
    "attack_name": "No Mercy",
    "damage": 30,
    "ap_cost": 30,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 67,
    "attack_name": "Ticking Bomb",
    "damage": 29,
    "ap_cost": 30,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 68,
    "attack_name": "Lie In Your Grave",
    "damage": 34,
    "ap_cost": 32,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 69,
    "attack_name": "Run This City",
    "damage": 28,
    "ap_cost": 30,
    "unique": false,
    "city": null
  },
  {
    "attack_id": 70,
    "attack_name": "FrostBite",
    "damage": 25,
    "ap_cost": 28,
    "unique": true,
    "city": "Research Station Alpha"
  }
];
/*
npc_character_attack.forEach(doc =>{
	NPC_Character_Attacks.insert(doc);
});*/

export default NPC_Character_Attacks;
