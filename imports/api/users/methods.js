Meteor.methods({
  doesUserExist(name)
  {
    return Accounts.findUserByUsername(name) != null;
  }
});
