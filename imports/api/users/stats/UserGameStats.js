import SimpleSchema from 'simpl-schema';
import { Mongo } from 'meteor/mongo';



UserStats = new Mongo.Collection('user_stats');

StageSchema = new SimpleSchema({
	name:
	{
		type: String
	},
	enemy_count:
	{
		type: Number
	},
	enemies_beat:
	{
		type: Array,
		label: "Enemies that user has beaten"
	},
	'enemies_beat.$':
	{
		type: String
	},
	wins:
	{
		type: Number,
		defaultValue: 0
	},
	losses:
	{
		type: Number,
		defaultValue: 0
	}
});



UserGameStatsSchema = new SimpleSchema({
	info:
	{
		type: Object
	},
	'info.user':
	{
		type: String

	},
	'info.stages':
	{
		type: Object
	},
	'info.stages.stage':
	{
		type: StageSchema
	},
	turns:
	{
		type: Number,
		defaultValue: 0
	}
	/*
	stages:
	{
		type: Object
	},
	'stages.stage':
	{
		type: StageSchema
	}
	*/

});



UserStats.schema = UserGameStatsSchema;

//export default UserStats;
