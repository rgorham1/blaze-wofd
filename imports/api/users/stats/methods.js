import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import './UserGameStats';

Meteor.methods({
	initiateWinLoss()
	{
		let userID = Meteor.userId();

		UserStats.insert({
			info: {user: userID},
			info: {stages: {stage: {wins}}}: 0,
			info.stages.stage.losses: 0
		});

	},
	initiateStats(stage)
	{
		check(stage, String);
		let userID = Meteor.userId();
		let enemy_count = Cities.find({continent: stage}).count();

		/*
		UserStats.upsert({
			user: userID
		},
		{
			$set: {
				"stage.name": stage,
				"stage.enemy_count": enemy_count,
				"stage.enemies_beat": [],
				"turns": 0
			}
		});
		*/



		UserStats.update(
			{ 'info.user': userID,
		 		'info.stages.stage.name': stage
			},
			{$set:
				{
					"info.stages.stage.enemy_count": enemy_count,
					"info.stages.stage.enemies_beat": [],
					"turns": 0
				}
			},
			{ upsert:true }
		);

	},
	getTurn()
	{
		let userID = Meteor.userId();
		let user_stats = UserStats.findOne({user: userID});

		return user_stats.turns;

	},
	//keep track of how many turns the user has used
	userAttackAction()
	{
		let counter;
		let userID = Meteor.userId();
		let user_stats = UserStats.findOne({user: userID});
		let turns = user_stats.turns;


		if (turns == 0) counter = 1;
		else counter = turns+=1;

		//console.log(counter);


		UserStats.update({
			user: userID},
			{ $set: { "turns": counter } }
		);

	},
	checkLuckyTurn()
	{
		let userID = Meteor.userId();
		let user_stats = UserStats.findOne({user: userID});
		let turn = user_stats.turns;
		let lucky_turn = false;

		if (turn === 3)
			lucky_turn = true;

		return lucky_turn;
	},
	resetTurns()
	{
		let userID = Meteor.userId();
		let user_stats = UserStats.findOne({user: userID});
		let turns = user_stats.turns;

		turns = 0;

	},
	//TODO: change this to totalWins
	//this will become stage wins
	getWins()
	{
		let userID = Meteor.userId();
		user_stats = UserStats.findOne({'info.user': userID});

		return user_stats.info.stages.stage.wins;
	},
	updateWins(wins)
	{
		check(wins, Number);
		let userID = Meteor.userId();
		UserStats.update(
			{'info.user': userID},
			{ $set: { "info.stages.stage.wins": wins } }
		);

	},
	//TODO: work on this
	// add enemies you have beaten to list to track how many enemies beat
	// and also use this information in character pool for npc to use
	winHistory(enemy)
	{
		check(enemy, String);
		let userID = Meteor.userId();


		UserStats.update(
			{'info.user': userID},
			{ $push: { "info.stages.stage.enemies_beat": enemy } }
		);

	},

	//change this to totalLosses
	getLosses()
	{
		let userID = Meteor.userId();
		user_stats = UserStats.findOne({'info.user': userID});


		return user_stats.info.stages.stage.losses;
	},
	updateLosses(losses)
	{
		check(losses, Number);
		let userID = Meteor.userId();
		UserStats.update(
			{'info.user': userID},
			{ $set: { "info.stages.stage.losses": losses } }
		);

	}

});
