import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Random } from 'meteor/random'

import './BaseCharacters';

Meteor.methods({
	getNPCTemplates()
	{

		npcTemplates = BaseCharacter.find({npc_flag: true}).fetch();  //get list of npc templates ie fast, lucky, etc.
  	return Random.choice(npcTemplates);
	},

	getMoralCharacters(morals)
	{
		let bc = BaseCharacter.find({
			good_evil_flag: {$in: [ morals , "both" ]}
		}).fetch();
		console.log(bc);
	}


});
