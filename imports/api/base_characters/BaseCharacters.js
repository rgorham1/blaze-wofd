import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';


BaseCharacter = new Mongo.Collection('base_character');

BaseCharacterSchema = new SimpleSchema({
	character_description:
	{
		type: String,
        label: "Character Type"

	},

	hp:
	{
		type: Number,
        label: "Character Health"
	},

	ap:
	{
		type: Number,
        label: "Character Energy"
	},

	speed:
	{
		type: Number,
        label: "Character's Speed"
	},

	strength:
	{
		type: Number,
        label: "Character's Strength"
	},

	luck:
	{
		type: Number,
        label: "Character's Luck"
	},

	intelligence:
	{
		type: Number,
        label: "Character's IQ"
	},

	chi:
	{
		type: Number,
        label: "Character's Chi"
	},

	stealth:
	{
		type: Number,
        label: "Character's Stealth"
	},

	good_evil_flag:
	{
		type: String,
        label: "Whether Character is Good or Bad"
	},

	npc_flag:
	{
		type: Boolean,
        label: "Whether Character can be NPC"
	},

	boss_flag:
	{
		type: Boolean,
        label: "Whether Character is Boss"
	}

});


BaseCharacter.schema = BaseCharacterSchema;

BaseCharacter.allow({
  insert: () => true,
  update: () => false,
  remove: () => false
});

BaseCharacter.deny({
  insert: () => false,
  update: () => true,
  remove: () => true
});



var base_character = [
  {
    "character_description": "Boxer",
    "hp": 88,
    "ap": 85,
    "speed": 3,
    "strength": 5,
    "luck": 2,
    "intelligence": 3,
    "chi": 2,
    "stealth": 1,
    "good_evil_flag": "good",
    "npc_flag": false,
    "boss_flag": false

  },
  {
    "character_description": "MMA",
    "hp": 88,
    "ap": 85,
    "speed": 3,
    "strength": 3,
    "luck": 2,
    "intelligence": 2,
    "chi": 3,
    "stealth": 3,
    "good_evil_flag": "both",
    "npc_flag": false,
    "boss_flag": false
  },
  {
    "character_description": "BJJ",
    "hp": 85,
    "ap": 85,
    "speed": 4,
    "strength": 2,
    "luck": 5,
    "intelligence": 2,
    "chi": 2,
    "stealth": 1,
    "good_evil_flag": "good",
    "npc_flag": false,
    "boss_flag": false
  },
  {
    "character_description": "Ninja",
    "hp": 83,
    "ap": 83,
    "speed": 5,
    "strength": 1,
    "luck": 1,
    "intelligence": 1,
    "chi": 2,
    "stealth": 6,
    "good_evil_flag": "evil",
    "npc_flag": false,
    "boss_flag": false
  },
  {
    "character_description": "Muay Thai",
    "hp": 92,
    "ap": 90,
    "speed": 4,
    "strength": 4,
    "luck": 2,
    "intelligence": 2,
    "chi": 3,
    "stealth": 1,
    "good_evil_flag": "evil",
    "npc_flag": false,
    "boss_flag": false
  },
  {
    "character_description": "Sambo",
    "hp": 100,
    "ap": 90,
    "speed": 1,
    "strength": 6,
    "luck": 3,
    "intelligence": 1,
    "chi": 4,
    "stealth": 1,
    "good_evil_flag": "evil",
    "npc_flag": false,
    "boss_flag": false
  },
  {
    "character_description": "Fast",
    "hp": 81,
    "ap": 88,
    "speed": 6,
    "strength": 1,
    "luck": 4,
    "intelligence": 0,
    "chi": 4,
    "stealth": 5,
    "good_evil_flag": "NULL",
    "npc_flag": true,
    "boss_flag": false
  },
  {
    "character_description": "Strong",
    "hp": 110,
    "ap": 95,
    "speed": 1,
    "strength": 7,
    "luck": 4,
    "intelligence": 0,
    "chi": 5,
    "stealth": 3,
    "good_evil_flag": "NULL",
    "npc_flag": true,
    "boss_flag": false
  },
  {
    "character_description": "Balanced",
    "hp": 93,
    "ap": 93,
    "speed": 4,
    "strength": 4,
    "luck": 4,
    "intelligence": 0,
    "chi": 4,
    "stealth": 4,
    "good_evil_flag": "NULL",
    "npc_flag": true,
    "boss_flag": false
  },
  {
    "character_description": "Lucky",
    "hp": 83,
    "ap": 85,
    "speed": 3,
    "strength": 2,
    "luck": 7,
    "intelligence": 0,
    "chi": 3,
    "stealth": 5,
    "good_evil_flag": "NULL",
    "npc_flag": true,
    "boss_flag": false
  },
  {
    "character_description": "AP",
    "hp": 82,
    "ap": 110,
    "speed": 3,
    "strength": 2,
    "luck": 5,
    "intelligence": 0,
    "chi": 7,
    "stealth": 3,
    "good_evil_flag": "NULL",
    "npc_flag": true,
    "boss_flag": false
  },
  {
    "character_description": "Commander Naked Head Basher",
    "hp": 150,
    "ap": 120,
    "speed": 6,
    "strength": 6,
    "luck": 10,
    "intelligence": 0,
    "chi": 7,
    "stealth": 4,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "King Pies",
    "hp": 160,
    "ap": 140,
    "speed": 6,
    "strength": 7,
    "luck": 7,
    "intelligence": 0,
    "chi": 8,
    "stealth": 7,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Brazilian MotherChoker",
    "hp": 130,
    "ap": 125,
    "speed": 9,
    "strength": 6,
    "luck": 5,
    "intelligence": 0,
    "chi": 7,
    "stealth": 5,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Chuck Tyson",
    "hp": 175,
    "ap": 160,
    "speed": 6,
    "strength": 10,
    "luck": 7,
    "intelligence": 0,
    "chi": 8,
    "stealth": 5,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Mockodile Crundee",
    "hp": 120,
    "ap": 125,
    "speed": 4,
    "strength": 6,
    "luck": 5,
    "intelligence": 0,
    "chi": 7,
    "stealth": 7,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Sir Joffrey",
    "hp": 110,
    "ap": 150,
    "speed": 5,
    "strength": 5,
    "luck": 9,
    "intelligence": 0,
    "chi": 7,
    "stealth": 8,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Golden Samurai",
    "hp": 150,
    "ap": 145,
    "speed": 6,
    "strength": 9,
    "luck": 5,
    "intelligence": 0,
    "chi": 8,
    "stealth": 4,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Lei Bruce",
    "hp": 180,
    "ap": 200,
    "speed": 10,
    "strength": 8,
    "luck": 8,
    "intelligence": 0,
    "chi": 7,
    "stealth": 4,
    "good_evil_flag": "NULL",
    "npc_flag": false,
    "boss_flag": true
  },
  {
    "character_description": "Monk",
    "hp": 80,
    "ap": 100,
    "speed": 2,
    "strength": 1,
    "luck": 3,
    "intelligence": 4,
    "chi": 5,
    "stealth": 1,
    "good_evil_flag": "good",
    "npc_flag": false,
    "boss_flag": false
  }
];

/*
base_character.forEach(doc =>{
	BaseCharacter.insert(doc);
});
*/


//export default BaseCharacter;
