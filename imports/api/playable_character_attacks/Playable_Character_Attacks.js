import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';


const Playable_Character_Attacks = new Mongo.Collection('playable_character_attacks');


CharacterAttacksSchema = new SimpleSchema({
  attack_id:
  {
    type: Number
  },

	attack_name:
	{
		type: String
	},

	damage:
	{
		type: Number
	},

	ap_cost:
	{
		type: Number
	},

	character:
	{
		type: String
	},

	unique:
	{
		type: Boolean
	}

});

Playable_Character_Attacks.schema = CharacterAttacksSchema;



var playable_attacks = [
  {
    "attack_id": 1,
    "attack_name": "Flying Dragon Fists",
    "damage": 10,
    "ap_cost": 14,
    "character": "Monk",
    "unique": true
  },
  {
    "attack_id": 2,
    "attack_name": "Bald Headed Eagle Strike",
    "damage": 18,
    "ap_cost": 15,
    "character": "Monk",
    "unique": true
  },
  {
    "attack_id": 3,
    "attack_name": "Shaolin Secret Move of Doom",
    "damage": 20,
    "ap_cost": 35,
    "character": "Monk",
    "unique": true
  },
  {
    "attack_id": 4,
    "attack_name": "Meditating Dragon",
    "damage": 20,
    "ap_cost": 30,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 5,
    "attack_name": "Iron Palm",
    "damage": 23,
    "ap_cost": 35,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 6,
    "attack_name": "18 Tigers",
    "damage": 18,
    "ap_cost": 22,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 7,
    "attack_name": "Meditation",
    "damage": 0,
    "ap_cost": 0,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 8,
    "attack_name": "Healing Prayer",
    "damage": 0,
    "ap_cost": 30,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 9,
    "attack_name": "Cleansing Mist",
    "damage": 15,
    "ap_cost": 20,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 10,
    "attack_name": "100 Palms Strike",
    "damage": 23,
    "ap_cost": 40,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 11,
    "attack_name": "Crane Fist",
    "damage": 18,
    "ap_cost": 15,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 12,
    "attack_name": "Tiger Palm",
    "damage": 10,
    "ap_cost": 25,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 13,
    "attack_name": "Dragon’s Wrath",
    "damage": 12,
    "ap_cost": 18,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 14,
    "attack_name": "18 Deadly Poles",
    "damage": 21,
    "ap_cost": 35,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 15,
    "attack_name": "36 Chambers of Death",
    "damage": 25,
    "ap_cost": 40,
    "character": "Monk",
    "unique": false
  },
  {
    "attack_id": 16,
    "attack_name": "Tattooed Face Ear Bite",
    "damage": 18,
    "ap_cost": 20,
    "character": "Boxer",
    "unique": true
  },
  {
    "attack_id": 17,
    "attack_name": "Floating Butterfly Jab",
    "damage": 8,
    "ap_cost": 13,
    "character": "Boxer",
    "unique": true
  },
  {
    "attack_id": 18,
    "attack_name": "Children Eater Haymaker",
    "damage": 22,
    "ap_cost": 25,
    "character": "Boxer",
    "unique": true
  },
  {
    "attack_id": 19,
    "attack_name": "Sting of the Bumblebee",
    "damage": 20,
    "ap_cost": 20,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 20,
    "attack_name": "Corner Rest",
    "damage": 20,
    "ap_cost": 20,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 21,
    "attack_name": "Double Uppercut",
    "damage": 21,
    "ap_cost": 19,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 22,
    "attack_name": "1-2 Combo",
    "damage": 21,
    "ap_cost": 22,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 23,
    "attack_name": "Body shot",
    "damage": 12,
    "ap_cost": 10,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 24,
    "attack_name": "Mayweather",
    "damage": 1,
    "ap_cost": 1,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 25,
    "attack_name": "Dancing Fists",
    "damage": 23,
    "ap_cost": 27,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 26,
    "attack_name": "Okie Doke",
    "damage": 27,
    "ap_cost": 20,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 27,
    "attack_name": "Straight Left",
    "damage": 13,
    "ap_cost": 12,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 28,
    "attack_name": "Lightning Hook",
    "damage": 28,
    "ap_cost": 30,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 29,
    "attack_name": "Below the Belt",
    "damage": 22,
    "ap_cost": 23,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 30,
    "attack_name": "Illegal Headbutt",
    "damage": 23,
    "ap_cost": 26,
    "character": "Boxer",
    "unique": false
  },
  {
    "attack_id": 31,
    "attack_name": "Face Crushing Kick",
    "damage": 15,
    "ap_cost": 18,
    "character": "MMA",
    "unique": true
  },
  {
    "attack_id": 32,
    "attack_name": "20 hit combo w/ Takedown",
    "damage": 20,
    "ap_cost": 22,
    "character": "MMA",
    "unique": true
  },
  {
    "attack_id": 33,
    "attack_name": "Sexy Armbar",
    "damage": 30,
    "ap_cost": 31,
    "character": "MMA",
    "unique": true
  },
  {
    "attack_id": 34,
    "attack_name": "Silverback Slam",
    "damage": 24,
    "ap_cost": 24,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 35,
    "attack_name": "Lights Out",
    "damage": 18,
    "ap_cost": 20,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 36,
    "attack_name": "Nap Time",
    "damage": 25,
    "ap_cost": 26,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 37,
    "attack_name": "Call Cut Man",
    "damage": 0,
    "ap_cost": 22,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 38,
    "attack_name": "Lay on Top of You",
    "damage": 0,
    "ap_cost": 1,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 39,
    "attack_name": "1-2 Combo",
    "damage": 21,
    "ap_cost": 22,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 40,
    "attack_name": "Body shot",
    "damage": 12,
    "ap_cost": 10,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 41,
    "attack_name": "Taste My Foot",
    "damage": 25,
    "ap_cost": 25,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 42,
    "attack_name": "40 Knees",
    "damage": 32,
    "ap_cost": 33,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 43,
    "attack_name": "Tornado Elbow",
    "damage": 18,
    "ap_cost": 18,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 44,
    "attack_name": "Tap Out Now",
    "damage": 23,
    "ap_cost": 24,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 45,
    "attack_name": "TKO",
    "damage": 24,
    "ap_cost": 25,
    "character": "MMA",
    "unique": false
  },
  {
    "attack_id": 46,
    "attack_name": "Blood Rushing Choke",
    "damage": 21,
    "ap_cost": 23,
    "character": "BJJ",
    "unique": true
  },
  {
    "attack_id": 47,
    "attack_name": "Never Gonna Let You Go",
    "damage": 22,
    "ap_cost": 25,
    "character": "BJJ",
    "unique": true
  },
  {
    "attack_id": 48,
    "attack_name": "You Better Tap",
    "damage": 28,
    "ap_cost": 30,
    "character": "BJJ",
    "unique": true
  },
  {
    "attack_id": 49,
    "attack_name": "Smother",
    "damage": 0,
    "ap_cost": 1,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 50,
    "attack_name": "Say Gracie",
    "damage": 22,
    "ap_cost": 25,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 51,
    "attack_name": "Leg Snap",
    "damage": 25,
    "ap_cost": 26,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 52,
    "attack_name": "Full Mount",
    "damage": 29,
    "ap_cost": 30,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 53,
    "attack_name": "Rest",
    "damage": 0,
    "ap_cost": 20,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 54,
    "attack_name": "Heel Lock",
    "damage": 24,
    "ap_cost": 22,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 55,
    "attack_name": "Take Your Arm Back",
    "damage": 22,
    "ap_cost": 20,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 56,
    "attack_name": "10 G Neck Crank",
    "damage": 27,
    "ap_cost": 25,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 57,
    "attack_name": "Can Opener",
    "damage": 25,
    "ap_cost": 23,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 58,
    "attack_name": "Wake Up Its Over Already",
    "damage": 30,
    "ap_cost": 30,
    "character": "BJJ",
    "unique": false
  },
  {
    "attack_id": 59,
    "attack_name": "Hidden Mist",
    "damage": 20,
    "ap_cost": 18,
    "character": "Ninja",
    "unique": true
  },
  {
    "attack_id": 60,
    "attack_name": "Blowgun Fury",
    "damage": 15,
    "ap_cost": 12,
    "character": "Ninja",
    "unique": true
  },
  {
    "attack_id": 61,
    "attack_name": "Silent Night w/ Stars",
    "damage": 27,
    "ap_cost": 29,
    "character": "Ninja",
    "unique": true
  },
  {
    "attack_id": 62,
    "attack_name": "Slap ''Em Silly",
    "damage": 18,
    "ap_cost": 18,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 63,
    "attack_name": "Typhoon",
    "damage": 28,
    "ap_cost": 29,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 64,
    "attack_name": "Mega Groin Kick",
    "damage": 25,
    "ap_cost": 24,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 65,
    "attack_name": "From the Ground Up",
    "damage": 22,
    "ap_cost": 24,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 66,
    "attack_name": "Walking on Air",
    "damage": 29,
    "ap_cost": 27,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 67,
    "attack_name": "Poison Breath",
    "damage": 32,
    "ap_cost": 30,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 68,
    "attack_name": "You Cant See Me",
    "damage": 29,
    "ap_cost": 25,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 69,
    "attack_name": "Snake Eyes Shadow",
    "damage": 21,
    "ap_cost": 18,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 70,
    "attack_name": "The Fifth Element",
    "damage": 19,
    "ap_cost": 15,
    "character": "Ninja",
    "unique": false
  },
  {
    "attack_id": 71,
    "attack_name": "Head buster",
    "damage": 29,
    "ap_cost": 27,
    "character": "Muay Thai",
    "unique": true
  },
  {
    "attack_id": 72,
    "attack_name": "Elephant Knee Strike",
    "damage": 11,
    "ap_cost": 10,
    "character": "Muay Thai",
    "unique": true
  },
  {
    "attack_id": 73,
    "attack_name": "100 Bones Breaker",
    "damage": 28,
    "ap_cost": 31,
    "character": "Muay Thai",
    "unique": true
  },
  {
    "attack_id": 74,
    "attack_name": "Ride the Elephant",
    "damage": 15,
    "ap_cost": 14,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 75,
    "attack_name": "Throw Hot Soup in Face",
    "damage": 15,
    "ap_cost": 15,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 76,
    "attack_name": "Stab With Big Knife",
    "damage": 17,
    "ap_cost": 15,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 77,
    "attack_name": "80 Elbows",
    "damage": 25,
    "ap_cost": 24,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 78,
    "attack_name": "Don’t Get Me Mad",
    "damage": 17,
    "ap_cost": 21,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 79,
    "attack_name": "Elbow Crazy",
    "damage": 22,
    "ap_cost": 23,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 80,
    "attack_name": "Flying Knee",
    "damage": 18,
    "ap_cost": 18,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 81,
    "attack_name": "Elephant Horns",
    "damage": 20,
    "ap_cost": 19,
    "character": "Muay Thai",
    "unique": false
  },
  {
    "attack_id": 82,
    "attack_name": "Ground Pounder",
    "damage": 32,
    "ap_cost": 33,
    "character": "Sambo",
    "unique": true
  },
  {
    "attack_id": 83,
    "attack_name": "Siberian Head Stomp",
    "damage": 30,
    "ap_cost": 30,
    "character": "Sambo",
    "unique": true
  },
  {
    "attack_id": 84,
    "attack_name": "Mother Russia",
    "damage": 29,
    "ap_cost": 31,
    "character": "Sambo",
    "unique": true
  },
  {
    "attack_id": 85,
    "attack_name": "The Dolph",
    "damage": 28,
    "ap_cost": 30,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 86,
    "attack_name": "Hulker",
    "damage": 25,
    "ap_cost": 27,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 87,
    "attack_name": "Blast of Radiation",
    "damage": 23,
    "ap_cost": 25,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 88,
    "attack_name": "Tombstone Piledriver",
    "damage": 18,
    "ap_cost": 18,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 89,
    "attack_name": "Take a breather",
    "damage": 0,
    "ap_cost": 1,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 90,
    "attack_name": "Lights Out",
    "damage": 27,
    "ap_cost": 30,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 91,
    "attack_name": "Bye Bye",
    "damage": 16,
    "ap_cost": 17,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 92,
    "attack_name": "Cant Stop Wont Stop",
    "damage": 18,
    "ap_cost": 24,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 93,
    "attack_name": "Super Fist",
    "damage": 21,
    "ap_cost": 23,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 94,
    "attack_name": "The Fedor",
    "damage": 29,
    "ap_cost": 30,
    "character": "Sambo",
    "unique": false
  },
  {
    "attack_id": 95,
    "attack_name": "10 Megaton Blast",
    "damage": 26,
    "ap_cost": 28,
    "character": "Sambo",
    "unique": false
  }
];

//console.log(playable_attacks.length);

export default Playable_Character_Attacks;

/*
playable_attacks.forEach(doc =>{
	Playable_Character_Attacks.insert(doc);
});
*/
