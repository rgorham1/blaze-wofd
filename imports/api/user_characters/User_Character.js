import SimpleSchema from 'simpl-schema';


UserCharacterSchema = new SimpleSchema({
	character:
	{
		type: String
	},

	hp:
	{
		type: Number
	},

	ap:
	{
		type: Number
	},

	speed:
	{
		type: Number
	},

	strength:
	{
		type: Number
	},

	luck:
	{
		type: Number
	},

	intelligence:
	{
		type: Number
	},

	chi:
	{
		type: Number
	},

	stealth:
	{
		type: Number
	},

	attack_1:
	{
		type: String
	},

	attack_2:
	{
		type: String
	},

	attack_3:
	{
		type: String
	},

	xp:
	{
		type: Number
	},
	wins:
	{
		type: Number
	},
	losses:
	{
		type: Number
	}
});