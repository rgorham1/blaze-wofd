import SimpleSchema from 'simpl-schema';


SimpleSchema.extendOptions(['autoform']);

MoralsSchema= new SimpleSchema({
  spirit_animal:
  {
    type: String,
    label: "Which is your spirit animal?",
    autoform: {
      type: 'select-radio',
      options: function(){
        return [
        {
          label: '2 head lion beast who breathes fire and has dragon teeth',
          value: 'evil'
        },
        {
          label: 'Chameleon monkey who spits water',
          value: 'good'
        }]
      }
    }

  },

  honor:
  {
    type: String,
    label: "You are handily beating your opponent and they are on the brink of defeat.",
    autoform: {
      type: 'select-radio',
      options: function(){
        return [
        {
          label: 'Finish them fatally',
          value: 'evil'
        },
        {
          label: 'Allow them mercy',
          value: 'good'
        }]
      }
    }
  },

  humility:
  {
    type: String,
    label: 'A stranger bumps into you while walking down a path.',
    autoform: {
      type: 'select-radio',
      options: function(){
        return [
        {
          label: 'Destroy them for even the idea of touching your greatness.',
          value: 'evil'
        },
        {
          label: 'Teach them the art of "walking down path.',
          value: 'good'
        }]
      }
    }

  },

  diplomacy:
  {
    type: String,
    label: 'War is coming',
    autoform: {
      type: 'select-radio',
      options: function(){
        return [
        {
          label: 'Relish the opportunity to hold your enemies head in your hand',
          value: 'evil'
        },
        {
          label: 'Hope for peace and diplomacy',
          value: 'good'
        }]
      }
    }
  },

  kindness:
  {
    type: String,
    label: 'There is an old man coming down path who is struggling and about to fall.',
    autoform: {
      type: 'select-radio',
      options: function(){
        return [
        {
          label: 'Take him down since he might be a grandmaster.',
          value: 'evil'
        },
        {
          label: 'Help him steady himself and walk him down the path.',
          value: 'good'
        }]
      }
    }
  },

  morality:
  {
    type: String
  }

});


