import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

Cities = new Mongo.Collection('cities');

CitiesSchema = new SimpleSchema({
	city:
	{
		type: String
	},

	continent:
	{
		type: String
	},

	style:
	{
		type: String,
		optional: true
	},

	character:
	{
		type: String,
		optional: true
	}
});

Cities.schema = CitiesSchema;

Cities.allow({
  insert: () => true,
  update: () => false,
  remove: () => false
});

Cities.deny({
  insert: () => false,
  update: () => true,
  remove: () => true
});

var geography_city = [
  {
    "city": "Research Station Alpha",
    "continent": "ANTARTICA",
    "style": "Genetics",
		"character": "Dr. Frostman"
  },
  {
    "city": "Frozen Rock",
    "continent": "ANTARTICA",
    "style": "Ice Style",
		"character": "Ice Guy"
  },
  {
    "city": "Peter Island",
    "continent": "ANTARTICA",
    "style": " ",
		"character": "Ben Fostershire"
  },
  {
    "city": "New Zealand",
    "continent": "AUSTRALIA",
    "style": "Taiaha",
		"character": "Demon of Tazmania"
  },
  {
    "city": "Sydney",
    "continent": "AUSTRALIA",
    "style": "Kangaroo Boxing",
		"character": "Long-Legged Killer"
  },
  {
    "city": "Tasmania",
    "continent": "AUSTRALIA",
    "style": " ",
		"character": "Chocolate"
  },
  {
    "city": "Okinawa",
    "continent": "ASIA",
    "style": "Karate",
		"character": "Praying Monster"
  },
  {
    "city": "Korea",
    "continent": "ASIA",
    "style": "Tae KwonDo",
		"character": "Donnie Yeoh"
  },
  {
    "city": "India",
    "continent": "ASIA",
    "style": "Gatka",
		"character": "Pony Tailed Fat Seagull"
  },
  {
    "city": "Shanghai",
    "continent": "ASIA",
    "style": "Wing Chun",
		"character": "Eatsan"
  },
  {
    "city": "Tokyo",
    "continent": "ASIA",
    "style": "Aikido",
		"character": "Prince Xianche"
  },
  {
    "city": "Mt. Fuji",
    "continent": "ASIA",
    "style": "Sumo",
		"character": "Jet Chan"
  },
  {
    "city": "Malaysia",
    "continent": "ASIA",
    "style": "Silat",
		"character": "Sandstorm"
  },
  {
    "city": "Temple",
    "continent": "ASIA",
    "style": "WuTang",
		"character": "Ghost Faced Chef"
  },
  {
    "city": "Israel",
    "continent": "ASIA",
    "style": "Krav Maga",
		"character": "Mr. Fubu"
  },
  {
    "city": "Chinatown",
    "continent": "ASIA",
    "style": "Tai Chi",
		"character": "Caesar"
  },
  {
    "city": "Hong Kong",
    "continent": "ASIA",
    "style": " ",
		"character": "Moshing Sandman"
  },
  {
    "city": "Beijing",
    "continent": "ASIA",
    "style": " ",
		"character": "Lion Spear"
  },
  {
    "city": "South Africa",
    "continent": "AFRICA",
    "style": "Nguni Stick Fighting",
		"character": "Bobado"
  },
  {
    "city": "Congo",
    "continent": "AFRICA",
    "style": "Mukumbusu",
		"character": "Pierre G."
  },
  {
    "city": "Zimbabwe",
    "continent": "AFRICA",
    "style": "Gwindulumutu",
		"character": "Erik Thunderborn"
  },
  {
    "city": "Liberia",
    "continent": "AFRICA",
    "style": " ",
		"character": "Twinkletoes"
  },
  {
    "city": "Nigeria",
    "continent": "AFRICA",
    "style": "Dambe",
		"character": "Cold Feather"
  },
  {
    "city": "Belgium",
    "continent": "EUROPE",
    "style": "Kickboxing",
		"character": "Sean Paul Dame Van"
  },
  {
    "city": "Paris",
    "continent": "EUROPE",
    "style": "Savate",
		"character": "General Clay"
  },
  {
    "city": "London",
    "continent": "EUROPE",
    "style": " ",
		"character": "25"
  },
  {
    "city": "Norway",
    "continent": "EUROPE",
    "style": "Stav",
		"character": "Kool Jimmy"
  },
  {
    "city": "Switzerland",
    "continent": "EUROPE",
    "style": "Fencing",
		"character": "One Eyed Pete"
  },
  {
    "city": "Siberia",
    "continent": "EUROPE",
    "style": "Systema",
		"character": "Lemon Peel"
  },
  {
    "city": "Scotland",
    "continent": "EUROPE",
    "style": "Shin Kicking",
		"character": "Canadian Punisher"
  },
  {
    "city": "NYC",
    "continent": "NORTH AMERICA",
    "style": "Wrestling",
		"character": "Shawn \"Cold Stone\" Johnson"
  },
  {
    "city": "Alcatraz",
    "continent": "NORTH AMERICA",
    "style": "Jailhouse Rock",
		"character": "Dudkoff"
  },
  {
    "city": "Boston",
    "continent": "NORTH AMERICA",
    "style": "Oom Yung Doe",
		"character": "Grace"
  },
  {
    "city": "West VA",
    "continent": "NORTH AMERICA",
    "style": "Eye Gouging",
		"character": "Chief Mountaintop"
  },
  {
    "city": "Chicago",
    "continent": "NORTH AMERICA",
    "style": "Street Fighting",
		"character": "Dancing Tortuga"
  },
  {
    "city": "NC",
    "continent": "NORTH AMERICA",
    "style": " ",
		"character": "Smoking Iguana"
  },
  {
    "city": "Montreal",
    "continent": "NORTH AMERICA",
    "style": "Okitchitaw",
		"character": "Paz"
  },
  {
    "city": "Las Vegas",
    "continent": "NORTH AMERICA",
    "style": "The Cane As A Weapon",
		"character": "Dan Black"
  },
  {
    "city": "LA",
    "continent": "NORTH AMERICA",
    "style": "Chun Kuk Do",
		"character": "Tommy Punk"
  },
  {
    "city": "Caritoba",
    "continent": "SOUTH AMERICA",
    "style": "Vale Tudo",
		"character": "Tuco"
  },
  {
    "city": "Venezuela",
    "continent": "SOUTH AMERICA",
    "style": "Vacon",
		"character": "Lord Clegane"
  },
  {
    "city": "Argentina",
    "continent": "SOUTH AMERICA",
    "style": "Capoeira",
		"character": "Ace"
  },
  {
    "city": "Lima",
    "continent": "SOUTH AMERICA",
    "style": "Rumi Maki",
		"character": "Vega"
  },
  {
    "city": "Rio de Janeiro",
    "continent": "SOUTH AMERICA",
    "style": " ",
		"character": "Prieto"
  }
];

export default Cities;
/*
geography_city.forEach(doc =>{
	Cities.insert(doc);
	});
	*/
