import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Random } from 'meteor/random';

import './Cities';

Meteor.methods({
	get_enemies(stage)
	{
		let enemies;
		let enemy_count;
		let user;


		enemy_count = Cities.find({continent: stage}).count();
		enemies = Cities.find({continent: stage}).fetch();
		console.log(enemy_count);
		console.log(enemies);

	},

	//gathers npc character name from list of possible characters based on stage
	getEnemy(stage)
	{
		npcOptions = Cities.find({continent: stage}).fetch(); //get list of possible enemies based on stage

		return Random.choice(npcOptions).character; //chosen character name;
	}

});
