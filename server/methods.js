import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Playable_Character_Attacks from '../imports/api/playable_character_attacks/Playable_Character_Attacks';
import Cities from '../imports/api/cities/Cities';
import BaseCharacter from '../imports/api/base_characters/BaseCharacters';
import  NPC_Character_Attacks from '../imports/api/npc_characters/NPC_Character_Attacks';
import '../imports/api/users/stats/UserGameStats';

//TODO: figure out best practice for meteor methods
// they seem to work when in server but not in imports/api/..
Meteor.methods({
	getMorals()
	{
		let current_user = Meteor.user();
		return current_user.profile.morality.morality;
	},

	getCurrentUser(){
		let current_user = Meteor.user();
		return current_user;
	},

	create_playable_character()
	{
		let user = Meteor.user();
		let user_character = user.profile.character;

		//player character object
		let user_object = {
	    char_name: user_character.character,
	    //stats
	    HP: user_character.HP,
	    AP: user_character.AP,
			maxHP: user_character.HP,
			maxAP: user_character.AP,
	    playerLevel: user_character.level,
	    playerXP: user_character.xp,
			wins: user_character.wins,
	    //attacks
	    playerAttacks: [user_character.attack_1, user_character.attack_2, user_character.attack_3],
	    //attributes
	    playerAttributes: {
	    	stealth: user_character.stealth,
	    	chi: user_character.chi,
	    	strength: user_character.strength,
	    	speed: user_character.speed,
	    	luck: user_character.luck,
	    	iq: user_character.intelligence
	    }
	  };

	  //get attack info and insert back into object
	  let attack1 = Playable_Character_Attacks.find( {attack_name: user_object.playerAttacks[0]}).fetch();
	  let attack1_info = {
	  	damage: attack1[0].damage,
	  	cost: attack1[0].ap_cost,
	  	name: attack1[0].attack_name
	  };

	  let attack2 = Playable_Character_Attacks.find( {attack_name: user_object.playerAttacks[1]}).fetch();
	  let attack2_info = {
	  	damage: attack2[0].damage,
	  	cost: attack2[0].ap_cost,
	  	name: attack2[0].attack_name
	  };

	  let attack3 = Playable_Character_Attacks.find( {attack_name: user_object.playerAttacks[2]}).fetch();
	  let attack3_info = {
	  	damage: attack3[0].damage,
	  	cost: attack3[0].ap_cost,
	  	name: attack3[0].attack_name
	  };

	  user_object.playerAttacks = [attack1_info, attack2_info, attack3_info];
	  //console.log(user_object);

	  return user_object;
	},

	/*
	check_npc_type(stage)
	{
		let enemies;
		let enemy_count;
		let npc_type;
		let user;
		let current_wins;

		enemies = Cities.find({continent: stage}).count();
		//console.log(enemies);

		user = Meteor.user();
		current_wins = user.profile.character.wins;
		if (current_wins > enemies){
			npc_type = 'boss';
		}
		else{
			npc_type = 'grunt';
		}

		//console.log(npc_type);

		return npc_type;
	},
	*/


	//TODO: need to work on boss logic and boss stages
	//boss location info doesnt seem to be in collections
	//picking which boss is supposed to fight
	create_npc(stage)
	{

		let npcOptions;
		let npcChoice;
		let npcName;
		let npcTemplates;
		let maxTemplates;

		npcOptions = Cities.find({continent: stage}).fetch();
		//for (i in npcOptions){
			//console.log(`possible choices are ${npcOptions[i].character}`);
		//}


		randLimit = npcOptions.length -1;
		//console.log(`max random number is ${randLimit}`);

		npcChoice = randomIntFromInterval(0, randLimit);
		//console.log(`random index is ${npcChoice}`);

		npcName = npcOptions[npcChoice].character;
		console.log(`chosen character is ${npcName}`);

		//get npc template ie fast, lucky, etc.
		npcTemplates = BaseCharacter.find({npc_flag: true}).fetch();

		maxTemplates = npcTemplates.length;
		//console.log(maxTemplates);
		templateChoice = randomIntFromInterval(0, maxTemplates -1);
		//console.log(templateChoice);

		npcTemplate = npcTemplates[templateChoice];
		//console.log(npcTemplate);

		//get attack information
		attackLimit = NPC_Character_Attacks.find({}).count();
		//console.log(attackLimit);

		let rand1;
		let rand2;
		let rand3;

		rand1 = randomIntFromInterval(1,attackLimit);
		rand2 = randomIntFromInterval(1,attackLimit);
		rand3 = randomIntFromInterval(1,attackLimit);
		//console.log(rand1, rand2, rand3);

		npcAttacks = NPC_Character_Attacks.find({ attack_id: { $in: [rand1,rand2, rand3] } }).fetch();
		//console.log(npcAttacks);

		const npcObject =
		{
			name: npcName,
			type: npcTemplate.character_description,
			HP: npcTemplate.hp,
			AP: npcTemplate.ap,
			maxHP: npcTemplate.hp,
			maxAP: npcTemplate.ap,
			npcAttacks: npcAttacks,
			npcAttributes:
			{
				speed: npcTemplate.speed,
				strength: npcTemplate.strength,
				luck: npcTemplate.luck,
				chi: npcTemplate.chi,
				iq: npcTemplate.intelligence,
				stealth: npcTemplate.stealth
			}

		};
		return npcObject;


	},

	//TODO:
	//check the geography_user_status table to determine how many attribute points to give to the npc
				//if they are on the first enemy then that npc will get enough attribute points to be 2 levels worse than the user character
				//if they are the second enemy then that npc will get enough attribute points to be 1 level worse than the user character
				//if they are the third or greater than they will be the same level as the user character



	

	

	

	

	
	/*
	initiateStats(stage)
	{
		
		UserStats.update({
			user: userID
		},
		{
			$set:
				{
					"stage.name": stage,
					"stage.enemy_count": enemy_count,
					"stage.enemies_beat": [],
					"stats.turns": 0
				}

		},
		{
			upsert:true
		});
		
	},
	
	
	*/


	insertPlayerChar(character)
	{
		check(character, {
			'character': String,
			'HP': Number,
			'AP': Number,
			'maxHP': Number,
			'maxAP': Number,
			'speed': Number,
			'strength': Number,
			'luck': Number,
			'intelligence': Number,
			'chi': Number,
			'stealth': Number
		});

		let user = Meteor.user();
		Meteor.users.update(
			user,
			{ $set: { "profile.character": character} }
    );
	}

	





});






//TODO: check out Meteor random method
//https://docs.meteor.com/packages/random.html
const randomIntFromInterval = (min,max) => Math.floor(Math.random()*(max-min+1)+min);
