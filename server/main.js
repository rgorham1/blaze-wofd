import { Meteor } from 'meteor/meteor';
import { Morals } from '../imports/api/morals/Morals';
//import { BaseCharacter } from '../imports/api/BaseCharacters';
import { Playable_Character_Attacks } from '../imports/api/playable_character_attacks/Playable_Character_Attacks';
//import { Cities } from '../imports/api/cities/Cities';
import { NPC_Character_Attacks } from '../imports/api/npc_characters/NPC_Character_Attacks';



import '../imports/api/users/stats/methods.js';
import '../imports/api/base_characters/methods.js';
import '../imports/api/cities/methods.js';

Meteor.startup(() => {
  // code to run on server at startup
});
