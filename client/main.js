import '../imports/ui/body.js';
import '../imports/startup/client/routes.js';
import '../imports/ui/help.js'
import '../imports/ui/register.js'
import '../imports/ui/story.js'
import '../imports/ui/test_screen.js'
import '../imports/ui/create_character.js'
import '../imports/ui/login.js'
import '../imports/ui/location.js'
import '../imports/ui/battle.js'
import '../imports/ui/summary.js'

import { Morals } from '../imports/api/morals/Morals';
import { BaseCharacter } from '../imports/api/base_characters/BaseCharacters';
import { Playable_Character_Attacks } from '../imports/api/playable_character_attacks/Playable_Character_Attacks';
import { Cities } from '../imports/api/cities/Cities';
import { NPC_Character_Attacks } from '../imports/api/npc_characters/NPC_Character_Attacks';
import { UserStats } from '../imports/api/users/stats/UserGameStats';
